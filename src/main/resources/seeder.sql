USE `pirislab1` ;

INSERT INTO `citizenship` (`id`, `name`) VALUES
(1, 'Belarus'),
(2, 'Russia');

INSERT INTO `city` (`id`, `name`) VALUES
(1, 'Minsk'),
(2, 'Vitebsk');

INSERT INTO `disability` (`id`, `name`) VALUES
  (1, 'First group'),
  (2, 'Second group'),
  (3, 'No');

INSERT INTO `marital_status` (`id`, `name`) VALUES
  (1, 'married'),
  (2, 'not married');

INSERT INTO `client` (`id`, `firstname`, `lastname`, `middlename`, `birthday`, `passport_number`, `issued_by`, `identity_number`, `birthplace`, `living_address`, `phone_number_home`, `phone_number_mobile`, `email`, `the_post`, `workplace`, `retired`, `income`, `living_city_id`, `residence_city_id`, `marital_status_id`, `citizenship_id`, `disability_id`, `passport_series`, `issued_date`) VALUES
(1, 'Vitaly', 'Orlov', 'Olegovich', '1996-09-14', '2168778', 'Lidsky ROVD, Grodnenskaya obl', '382983287878787813', 'Chernihiv, Ukraine', 'Lida, Gastello street, 49-1-38', '75-94-23', '+375292689950', 'vitalyorlov@mail.ru', '', '', 0, NULL, 2, 1, 2, 1, 3, 'KH', '2016-09-14'),
(3, 'Shilin', 'Jenka', 'Batkovich', '2000-02-03', '2168776', 'Vitebskii ROVD', '3829832878787', 'Vitebsk', 'Lenin street, building 5', '2-98-25', '+375298765432', 'jenka.ukrotitel.bab@mail.ru', 'friend', 'Paralect', 0, '201', 1, 1, 1, 1, 3, 'KH', '2015-02-03'),
(4, 'Shushkevic', 'Artem', 'Batkovich', '2000-02-03', '2910321', 'Lidsky ROVD, Grodnenskaya obl', '382983287878787821', 'London', 'Main street, building 6', '2-98-25', '', '', '', '', 0, NULL, 1, 1, 1, 2, 1, 'KH', '2016-09-14'),
(6, 'Dimka', 'Soroko', 'Batkovich', '2015-12-12', '9209193', 'Lidsky ROVD, Grodnenskaya obl', '03929039019039', 'place', 'address', '23-12-12', '', '', '', '', 0, NULL, 1, 2, 2, 1, 3, 'KH', '2012-03-03'),
(7, 'Alexey', 'Palto', 'Batkovich', '2011-02-21', '2901099', 'jjkajkda', '28919829819828', 'djakjdjs', 'sdkjajksd', '29-29-33', '', '', '', '', 0, NULL, 1, 2, 1, 2, 1, 'KH', '2015-01-03'),
(8, 'Dajsj', 'KDJksa', 'djaksjd', '2012-03-03', '3898283', 'jkdfkjs', '83892893898283', 'jdkajkjk', 'jdsjajk', '', '', '', '', '', 0, NULL, 1, 1, 1, 1, 1, 'KH', '2001-02-02');

INSERT INTO `plan_of_accounts` (`id`, `code_plan`, `name_plan`, `activity`) VALUES
  (1, 3014, 'current account for natural person', 'passive'),
  (2, 3012, 'current account for juridical ', 'passive'),
  (3, 3013, 'curent account for individual ', 'passive'),
  (4, 1010, 'cashbox in bank', 'active'),
  (5, 7327, 'fund development bank', 'passive'),
  (6, 2400, 'credit score for natural person', 'active'),
  (7, 2100, 'current account for juridical', 'active'),
  (8, 2300, 'curent account for individual', 'active');

INSERT INTO `currency` (`id`, `name`) VALUES (NULL, 'USD'), (NULL, 'EUR'), (NULL, 'BYN');