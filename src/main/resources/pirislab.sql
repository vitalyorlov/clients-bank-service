-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema pirislab1
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `pirislab1` ;

-- -----------------------------------------------------
-- Schema pirislab1
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `pirislab1` DEFAULT CHARACTER SET utf8 ;
USE `pirislab1` ;

-- -----------------------------------------------------
-- Table `pirislab1`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pirislab1`.`city` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pirislab1`.`marital_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pirislab1`.`marital_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pirislab1`.`citizenship`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pirislab1`.`citizenship` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pirislab1`.`disability`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pirislab1`.`disability` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pirislab1`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pirislab1`.`client` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(100) NOT NULL,
  `lastname` VARCHAR(100) NOT NULL,
  `middlename` VARCHAR(100) NOT NULL,
  `birthday` DATE NOT NULL,
  `passport_number` VARCHAR(100) NOT NULL,
  `issued_by` VARCHAR(200) NOT NULL,
  `identity_number` VARCHAR(100) NOT NULL,
  `birthplace` VARCHAR(100) NOT NULL,
  `living_address` VARCHAR(200) NOT NULL,
  `phone_number_home` VARCHAR(50) NULL,
  `phone_number_mobile` VARCHAR(50) NULL,
  `email` VARCHAR(200) NULL,
  `the_post` VARCHAR(100) NULL,
  `workplace` VARCHAR(200) NULL,
  `retired` TINYINT(1) NOT NULL,
  `income` DECIMAL NULL,
  `living_city_id` INT NOT NULL,
  `residence_city_id` INT NOT NULL,
  `marital_status_id` INT NOT NULL,
  `citizenship_id` INT NOT NULL,
  `disability_id` INT NOT NULL,
  `passport_series` VARCHAR(10) NOT NULL,
  `issued_date` DATE NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `passport_number_UNIQUE` (`passport_number` ASC),
  UNIQUE INDEX `identity_number_UNIQUE` (`identity_number` ASC),
  INDEX `fk_client_city_idx` (`living_city_id` ASC),
  INDEX `fk_client_city1_idx` (`residence_city_id` ASC),
  INDEX `fk_client_marital_status1_idx` (`marital_status_id` ASC),
  INDEX `fk_client_citizenship1_idx` (`citizenship_id` ASC),
  INDEX `fk_client_disability1_idx` (`disability_id` ASC),
  CONSTRAINT `fk_client_city`
  FOREIGN KEY (`living_city_id`)
  REFERENCES `pirislab1`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_client_city1`
  FOREIGN KEY (`residence_city_id`)
  REFERENCES `pirislab1`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_client_marital_status1`
  FOREIGN KEY (`marital_status_id`)
  REFERENCES `pirislab1`.`marital_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_client_citizenship1`
  FOREIGN KEY (`citizenship_id`)
  REFERENCES `pirislab1`.`citizenship` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_client_disability1`
  FOREIGN KEY (`disability_id`)
  REFERENCES `pirislab1`.`disability` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pirislab1`.`currency`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pirislab1`.`currency` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pirislab1`.`plan_of_accounts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pirislab1`.`plan_of_accounts` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code_plan` INT NOT NULL,
  `name_plan` VARCHAR(45) NOT NULL,
  `activity` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pirislab1`.`card`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pirislab1`.`card` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `number_card` VARCHAR(45) NOT NULL,
  `pin` INT NOT NULL,
  `attempt` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pirislab1`.`credit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pirislab1`.`credit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `number_agreement` INT NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `sum` INT NOT NULL,
  `percent` INT NOT NULL,
  `duration` INT NOT NULL,
  `start` DATETIME NOT NULL,
  `extend` DATETIME NOT NULL,
  `currency_id` INT NOT NULL,
  `client_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_credit_currency1_idx` (`currency_id` ASC),
  INDEX `fk_credit_client1_idx` (`client_id` ASC),
  CONSTRAINT `fk_credit_currency1`
  FOREIGN KEY (`currency_id`)
  REFERENCES `pirislab1`.`currency` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_credit_client1`
  FOREIGN KEY (`client_id`)
  REFERENCES `pirislab1`.`client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pirislab1`.`deposit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pirislab1`.`deposit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `number_agreement` INT NOT NULL,
  `type_deposit` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `sum` INT NOT NULL,
  `percent` INT NOT NULL,
  `duration` INT NOT NULL,
  `start` DATETIME NOT NULL,
  `extend` DATETIME NOT NULL,
  `currency_id` INT NOT NULL,
  `client_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_deposit_currency1_idx` (`currency_id` ASC),
  INDEX `fk_deposit_client1_idx` (`client_id` ASC),
  CONSTRAINT `fk_deposit_currency1`
  FOREIGN KEY (`currency_id`)
  REFERENCES `pirislab1`.`currency` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_deposit_client1`
  FOREIGN KEY (`client_id`)
  REFERENCES `pirislab1`.`client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `pirislab1`.`account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `pirislab1`.`account` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code_plan` INT NOT NULL,
  `number_account` VARCHAR(45) NOT NULL,
  `type_account` VARCHAR(45) NOT NULL,
  `activity_plan` VARCHAR(45) NOT NULL,
  `debet` VARCHAR(45) NOT NULL,
  `credit` VARCHAR(45) NOT NULL,
  `balance` VARCHAR(45) NOT NULL,
  `client_id` INT NOT NULL,
  `credit_id` INT NOT NULL,
  `deposit_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_account_client1_idx` (`client_id` ASC),
  INDEX `fk_account_credit1_idx` (`credit_id` ASC),
  INDEX `fk_account_deposit1_idx` (`deposit_id` ASC),
  CONSTRAINT `fk_account_client1`
  FOREIGN KEY (`client_id`)
  REFERENCES `pirislab1`.`client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_account_credit1`
  FOREIGN KEY (`credit_id`)
  REFERENCES `pirislab1`.`credit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_account_deposit1`
  FOREIGN KEY (`deposit_id`)
  REFERENCES `pirislab1`.`deposit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
