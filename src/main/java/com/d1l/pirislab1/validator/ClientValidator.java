package com.d1l.pirislab1.validator;

import com.d1l.pirislab1.model.Client;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClientValidator {

    public static boolean validate(Client client, StringBuilder errorMessage) {
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        Pattern p = Pattern.compile("^[A-Za-z]{1,100}$");
        Matcher m = p.matcher(client.getFirstname());
        if (!m.matches())
        {
            errorMessage.append("Firstname is incorrect\n");
            return false;
        }
        m = p.matcher(client.getLastname());
        if (!m.matches())
        {
            errorMessage.append("Lastname is incorrect\n");
            return false;
        }
        m = p.matcher(client.getMiddlename());
        if (!m.matches())
        {
            errorMessage.append("Middlename is incorrect\n");
            return false;
        }
        m = p.matcher(client.getPassportSeries());
        if (!m.matches())
        {
            errorMessage.append("Passport is incorrect\n");
            return false;
        }
        p = Pattern.compile("^[0-9]{1,100}$");
        m = p.matcher(client.getPassportNumber());
        if (!m.matches())
        {
            errorMessage.append("Passport number is incorrect\n");
            return false;
        }
        p = Pattern.compile("^[0-9A-Za-z]{1,100}$");
        m = p.matcher(client.getIdentityNumber());
        if (!m.matches())
        {
            errorMessage.append("Identity number is incorrect\n");
            return false;
        }
        p = Pattern.compile("^[0-9\\-+\\s]{0,100}$");
        m = p.matcher(client.getPhoneNumberHome());
        if (!m.matches())
        {
            errorMessage.append("Home phone number is incorrect\n");
            return false;
        }
        m = p.matcher(client.getPhoneNumberMobile());
        if (!m.matches())
        {
            errorMessage.append("Mobile phone number is incorrect\n");
            return false;
        }
        p = Pattern.compile("^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$");
        m = p.matcher(formatter.format(client.getBirthday()));
        if (!m.matches())
        {
            errorMessage.append("Birth date is incorrect\n");
            return false;
        }
        m = p.matcher(formatter.format(client.getIssuedDate()));
        if (!m.matches())
        {
            errorMessage.append("Issued date is incorrect\n");
            return false;
        }

        return true;
    }



}
