package com.d1l.pirislab1.util;

import com.d1l.pirislab1.model.Account;
import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;

import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.util.LinkedList;
import java.util.List;

public class PrintHelper {
    private static final Font FONT_FOR_OBJECT_NAME = FontFactory.getFont(FontFactory.HELVETICA, 20,
            Font.BOLD);
    private static final Font COMMON_FONT = FontFactory.getFont(FontFactory.HELVETICA, 20);

    private static void addWaterMark(PdfWriter writer) {
        Phrase watermark = new Phrase("Online Banking Systems", FontFactory.getFont(FontFactory.HELVETICA, 20,
                Font.BOLD, Color.LIGHT_GRAY));
        Rectangle pageSize = writer.getPageSize();
        float x = (pageSize.getLeft() + pageSize.getRight()) / 2;
        float y = (pageSize.getTop() + pageSize.getBottom()) / 2;
        PdfContentByte canvas = writer.getDirectContentUnder();
        ColumnText.showTextAligned(canvas, Element.ALIGN_CENTER, watermark, x, y, 45);
    }

    private static List<String> setAccountsRow(Account account) {
        List<String> accountRow = new LinkedList<String>();
        accountRow.add(String.format("%s", account.getNumberAccount()));
        accountRow.add(String.format("%s ", account.getTypeAccount()));
        accountRow.add(String.format("%s ", account.getBalance()));
        if (account.getCred() != null) accountRow.add(String.format("%s ", account.getCred().getCurrency().getName()));
        if (account.getDeposit() != null) accountRow.add(String.format("%s ", account.getDeposit().getCurrency().getName()));
        return accountRow;
    }

    public static ByteArrayOutputStream generateAccountInfoById(Account account) {
        Document document = new Document(PageSize.A6, 30, 20, 20, 30);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PdfWriter pdfWriter = null;
        try {
            pdfWriter = PdfWriter.getInstance(document, stream);
            document.open();
            addWaterMark(pdfWriter);
            List<String> accountsRow = setAccountsRow(account);

            Paragraph accountNumber = new Paragraph();
            accountNumber.add(new Chunk("AccountNumber #", FONT_FOR_OBJECT_NAME));
            accountNumber.add(new Chunk(accountsRow.get(0), COMMON_FONT));
            accountNumber.setAlignment(Element.ALIGN_CENTER);
            document.add(accountNumber);
            document.add(Chunk.NEWLINE);
            Paragraph type = new Paragraph();
            type.add(new Chunk("Type of account: ", FONT_FOR_OBJECT_NAME));
            type.add(new Chunk(accountsRow.get(1), COMMON_FONT));
            document.add(type);
            document.add(Chunk.NEWLINE);
            Paragraph balance = new Paragraph();
            balance.add(new Chunk("Balance: ", FONT_FOR_OBJECT_NAME));
            balance.add(new Chunk(accountsRow.get(2), COMMON_FONT));
            document.add(balance);
            document.add(Chunk.NEWLINE);
            Paragraph currency = new Paragraph();
            currency.add(new Chunk("Currency: ", FONT_FOR_OBJECT_NAME));
            currency.add(new Chunk(accountsRow.get(3), COMMON_FONT));
            document.add(currency);
            document.add(Chunk.NEWLINE);
            document.addAuthor("Online Banking Service");
        } catch (DocumentException e) {
            e.printStackTrace();
        } finally {
            if (document != null) {
                document.close();
            }
            if (pdfWriter != null) {
                document.close();
            }
        }
        return stream;
    }

    public static ByteArrayOutputStream generateGetMoneyReport(Account account, double sum) {
        Document document = new Document(PageSize.A6, 30, 20, 20, 30);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PdfWriter pdfWriter = null;
        try {
            pdfWriter = PdfWriter.getInstance(document, stream);
            document.open();
            addWaterMark(pdfWriter);

            List<String> accountsRow = setAccountsRow(account);

            Paragraph accountNumber = new Paragraph();
            accountNumber.add(new Chunk("AccountNumber #", FONT_FOR_OBJECT_NAME));
            accountNumber.add(new Chunk(accountsRow.get(0), COMMON_FONT));
            accountNumber.setAlignment(Element.ALIGN_CENTER);
            document.add(accountNumber);
            document.add(Chunk.NEWLINE);
            Paragraph sumAccount = new Paragraph();
            sumAccount.add(new Chunk("Sum: ", FONT_FOR_OBJECT_NAME));
            sumAccount.add(new Chunk(String.valueOf(sum), COMMON_FONT));
            sumAccount.setAlignment(Element.ALIGN_CENTER);
            document.add(sumAccount);
            document.add(Chunk.NEWLINE);
            document.addAuthor("Online Banking Service");
        } catch (DocumentException e) {
            e.printStackTrace();
        } finally {
            if (document != null) {
                document.close();
            }
            if (pdfWriter != null) {
                document.close();
            }
        }
        return stream;
    }

}
