package com.d1l.pirislab1.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CalcHelper {

    public static String createAccount(int lengthAccount, int codePlan)
    {
        List<Integer> masTailAccount = new ArrayList<Integer>();

        for(int i = 0; i < lengthAccount; i++)
        {
            masTailAccount.add(randInt(0, 9));
        }

        StringBuilder accountNumber = new StringBuilder();

        accountNumber.append(String.valueOf(codePlan));
        for (Integer number : masTailAccount) {
            accountNumber.append(number);
        }

        return accountNumber.toString();
    }

    private static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public static double calculateDebetAmmount (String activityPlan, double sumDeposit)
    {
        if (activityPlan.equals("active")) return sumDeposit;
        else return -1 * sumDeposit;
    }

    public static double calculateCreditAmmount (String activityPlan, double sumDeposit)
    {
        if (activityPlan.equals("active")) return -1 * sumDeposit;
        else return sumDeposit;
    }
}
