package com.d1l.pirislab1.model;

import javax.persistence.*;

@Entity
@Table(name = "pirislab1.plan_of_accounts")
public class AccountPlan {
    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "code_plan")
    private int codePlan;
    @Column(name = "name_plan")
    private String name;
    @Column(name = "activity")
    private String activity;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCodePlan() {
        return codePlan;
    }

    public void setCodePlan(int codePlan) {
        this.codePlan = codePlan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }
}
