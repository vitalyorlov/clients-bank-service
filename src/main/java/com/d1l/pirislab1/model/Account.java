package com.d1l.pirislab1.model;

import org.hibernate.annotations.FetchMode;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;

@Entity
@Table(name = "pirislab1.account")
public class Account {
    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "code_plan")
    private int codePlan;
    @Column(name = "number_account")
    private String numberAccount;
    @Column(name = "type_account")
    private String typeAccount;
    @Column(name = "activity_plan")
    private String activityPlan;
    @Column(name = "debet")
    private String debet;
    @Column(name = "credit")
    private String credit;
    @Column(name = "balance")
    private String balance;
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name="deposit_id")
    private Deposit deposit;
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name="credit_id")
    private Credit cred;
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name="client_id")
    private Client client;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCodePlan() {
        return codePlan;
    }

    public void setCodePlan(int codePlan) {
        this.codePlan = codePlan;
    }

    public String getNumberAccount() {
        return numberAccount;
    }

    public void setNumberAccount(String numberAccount) {
        this.numberAccount = numberAccount;
    }

    public String getTypeAccount() {
        return typeAccount;
    }

    public void setTypeAccount(String typeAccount) {
        this.typeAccount = typeAccount;
    }

    public String getActivityPlan() {
        return activityPlan;
    }

    public void setActivityPlan(String activityPlan) {
        this.activityPlan = activityPlan;
    }

    public String getDebet() {
        return debet;
    }

    public void setDebet(String debet) {
        this.debet = debet;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    @Transactional
    public Deposit getDeposit() {
        return deposit;
    }

    public void setDeposit(Deposit deposit) {
        this.deposit = deposit;
    }

    @Transactional
    public Credit getCred() {
        return cred;
    }

    public void setCred(Credit cred) {
        this.cred = cred;
    }

    @Transactional
    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
