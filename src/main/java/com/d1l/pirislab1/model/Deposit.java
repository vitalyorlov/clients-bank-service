package com.d1l.pirislab1.model;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "pirislab1.deposit")
public class Deposit {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private long id;
    @Column(name = "number_agreement")
    private int numberAgreement;
    @Column(name = "type_deposit")
    private String type;
    @Column(name = "name")
    private String name;
    @Column(name = "sum")
    private String sum;
    @Column(name = "percent")
    private String percent;
    @Column(name = "duration")
    private int duration;
    @Column(name = "start")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date start;
    @Column(name = "extend")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date extend;
    @ManyToOne(optional = false)
    @JoinColumn(name = "currency_id")
    private Currency currency;
    @ManyToOne(optional = false)
    @JoinColumn(name="client_id")
    private Client client;
    @Transient
    private int accountPlanID;

    public int getAccountPlanID() {
        return accountPlanID;
    }

    public void setAccountPlanID(int accountPlanID) {
        accountPlanID = accountPlanID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNumberAgreement() {
        return numberAgreement;
    }

    public void setNumberAgreement(int numberAgreement) {
        this.numberAgreement = numberAgreement;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getExtend() {
        return extend;
    }

    public void setExtend(Date extend) {
        this.extend = extend;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
