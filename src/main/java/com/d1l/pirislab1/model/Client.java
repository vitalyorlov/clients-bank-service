package com.d1l.pirislab1.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "pirislab1.client")
public class Client {
    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "firstname")
    private String firstname;
    @Column(name = "lastname")
    private String lastname;
    @Column(name = "middlename")
    private String middlename;
    @Column(name = "birthday")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date birthday;
    @Column(name = "passport_number")
    private String passportNumber;
    @Column(name = "issued_by")
    private String issuedBy;
    @Column(name = "identity_number")
    private String identityNumber;
    @Column(name = "birthplace")
    private String birthplace;
    @Column(name = "living_address")
    private String livingAddress;
    @Column(name = "phone_number_home")
    private String phoneNumberHome;
    @Column(name = "phone_number_mobile")
    private String phoneNumberMobile;
    @Column(name = "email")
    private String email;
    @Column(name = "the_post")
    private String thePost;
    @Column(name = "retired")
    private boolean retired;
    @Column(name = "workplace")
    private String workplace;
    @Column(name = "income")
    private BigDecimal income;
    @Column(name = "passport_series")
    private String passportSeries;
    @Column(name = "issued_date")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date issuedDate;
    @ManyToOne(optional = false)
    @JoinColumn(name="citizenship_id")
    private Citizenship citizenship;
    @ManyToOne(optional = false)
    @JoinColumn(name="living_city_id")
    private City livingCity;
    @ManyToOne(optional = false)
    @JoinColumn(name="residence_city_id")
    private City residenceCity;
    @ManyToOne(optional = false)
    @JoinColumn(name="disability_id")
    private Disability disability;
    @ManyToOne(optional = false)
    @JoinColumn(name="marital_status_id")
    private MaritalStatus maritalStatus;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public boolean isRetired() {
        return retired;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    public String getIssuedBy() {
        return issuedBy;
    }

    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String getLivingAddress() {
        return livingAddress;
    }

    public void setLivingAddress(String livingAddress) {
        this.livingAddress = livingAddress;
    }

    public String getPhoneNumberHome() {
        return phoneNumberHome;
    }

    public void setPhoneNumberHome(String phoneNumberHome) {
        this.phoneNumberHome = phoneNumberHome;
    }

    public String getPhoneNumberMobile() {
        return phoneNumberMobile;
    }

    public void setPhoneNumberMobile(String phoneNumberMobile) {
        this.phoneNumberMobile = phoneNumberMobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getThePost() {
        return thePost;
    }

    public void setThePost(String thePost) {
        this.thePost = thePost;
    }

    public boolean getRetired() {
        return retired;
    }

    public void setRetired(boolean retired) {
        this.retired = retired;
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public String getPassportSeries() {
        return passportSeries;
    }

    public void setPassportSeries(String passportSeries) {
        this.passportSeries = passportSeries;
    }

    public Date getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(Date issuedDate) {
        this.issuedDate = issuedDate;
    }

    public Citizenship getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(Citizenship citizenship) {
        this.citizenship = citizenship;
    }

    public City getLivingCity() {
        return livingCity;
    }

    public void setLivingCity(City livingCity) {
        this.livingCity = livingCity;
    }

    public City getResidenceCity() {
        return residenceCity;
    }

    public void setResidenceCity(City residenceCity) {
        this.residenceCity = residenceCity;
    }

    public Disability getDisability() {
        return disability;
    }

    public void setDisability(Disability disability) {
        this.disability = disability;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getFullname() {
        return this.lastname + ' ' + this.firstname + ' ' + this.middlename;
    }
}
