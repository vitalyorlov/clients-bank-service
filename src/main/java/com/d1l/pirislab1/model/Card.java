package com.d1l.pirislab1.model;

import javax.persistence.*;

@Entity
@Table(name = "pirislab1.card")
public class Card {
    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "number_card")
    private String numberCard;
    @Column(name = "pin")
    private int pin;
    @Column(name = "attempt")
    private int attempt;
    @Column(name = "client_id")
    private int clientId;
    @Column(name = "credit_id")
    private int creditId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumberCard() {
        return numberCard;
    }

    public void setNumberCard(String numberCard) {
        this.numberCard = numberCard;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public int getAttempt() {
        return attempt;
    }

    public void setAttempt(int attempt) {
        this.attempt = attempt;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getCreditId() {
        return creditId;
    }

    public void setCreditId(int creditId) {
        this.creditId = creditId;
    }
}
