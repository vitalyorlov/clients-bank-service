package com.d1l.pirislab1.model;

import javax.persistence.*;

@Entity
@Table(name = "pirislab1.city")
public class City {
    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "name")
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
