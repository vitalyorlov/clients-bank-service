package com.d1l.pirislab1.service;

import com.d1l.pirislab1.dao.ClientDao;
import com.d1l.pirislab1.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    private ClientDao clientDao;

    public void setClientDao(ClientDao clientDao) {
        this.clientDao = clientDao;
    }

    @Transactional
    public void addClient(Client c) {
        this.clientDao.addClient(c);
    }

    @Transactional
    public void updateClient(Client c) {
        this.clientDao.updateClient(c);
    }

    @Transactional
    public List<Client> listClients() {
        return this.clientDao.listClients();
    }

    @Transactional
    public Client getClientById(long id) {
        return this.clientDao.getClientById(id);
    }

    @Transactional
    public void removeClient(long id) {
        this.clientDao.removeClient(id);
    }

    public Client getClientByFullname(String firstname, String lastname, String middlename) {
        return this.clientDao.getClientByFullname(firstname, lastname, middlename);
    }

    public Client getClientByPassport(String passportSeries, String passportNumber) {
        return this.clientDao.getClientByPassport(passportSeries, passportNumber);
    }

    public Client getClientByIdentityNumber(String identityNumber) {
        return this.clientDao.getClientByIdentityNumber(identityNumber);
    }

}
