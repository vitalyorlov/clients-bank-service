package com.d1l.pirislab1.service;

import com.d1l.pirislab1.dao.AccountDao;
import com.d1l.pirislab1.model.Account;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    private AccountDao accountDao;

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Transactional
    public void addAccount(Account account) {
        this.accountDao.addAccount(account);
    }

    @Transactional
    public void updateAccount(Account account) {
        this.accountDao.updateAccount(account);
    }

    @Transactional
    public List<Account> listAccounts() {
        return this.accountDao.listAccounts();
    }

    @Transactional
    public Account getAccountById(int id) {
        return this.accountDao.getAccountById(id);
    }

    @Transactional
    public void removeAccount(int id) {
        this.accountDao.removeAccount(id);
    }

    @Transactional
    public Account getAccountByDepositId(int depositId) {
        return this.accountDao.getAccountByDepositId(depositId);
    }

    @Transactional
    public Account getMainAccountByDepositId(int depositId) {
        return this.accountDao.getAccountByDepositId(depositId);
    }

    @Transactional
    public Account getAccountByCreditId(int creditId) {
        return this.accountDao.getAccountByCreditId(creditId);
    }

    @Transactional
    public Account getMainAccountByCreditId(int creditId) {
        return this.accountDao.getMainAccountByCreditId(creditId);
    }

    @Transactional
    public List<Account> getDepositAccountsByClientId(int clientId) {return this.accountDao.getDepositAccountsByClientId(clientId);}
}
