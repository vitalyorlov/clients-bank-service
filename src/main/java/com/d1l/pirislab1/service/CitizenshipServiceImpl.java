package com.d1l.pirislab1.service;

import com.d1l.pirislab1.dao.CitizenshipDao;
import com.d1l.pirislab1.model.Citizenship;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CitizenshipServiceImpl implements CitizenshipService {

    private CitizenshipDao citizenshipDao;

    public void setCitizenshipDao(CitizenshipDao citizenshipDao) {
        this.citizenshipDao = citizenshipDao;
    }

    @Transactional
    public void addCitizenship(Citizenship c) {
        this.citizenshipDao.addCitizenship(c);
    }

    @Transactional
    public void updateCitizenship(Citizenship c) {
        this.citizenshipDao.updateCitizenship(c);
    }

    @Transactional
    public List<Citizenship> listCitizenships() {
        return this.citizenshipDao.listCitizenships();
    }

    @Transactional
    public Citizenship getCitizenshipById(int id) {
        return this.citizenshipDao.getCitizenshipById(id);
    }

    @Transactional
    public void removeCitizenship(int id) {
        this.citizenshipDao.removeCitizenship(id);
    }

}
