package com.d1l.pirislab1.service;

import com.d1l.pirislab1.model.Client;

import java.util.List;

public interface ClientService {
    public void addClient(Client c);
    public void updateClient(Client c);
    public List<Client> listClients();
    public Client getClientById(long id);
    public void removeClient(long id);
    public Client getClientByFullname(String firstname, String lastname, String middlename);
    public Client getClientByPassport(String passportSeries, String passportNumber);
    public Client getClientByIdentityNumber(String identityNumber);
}
