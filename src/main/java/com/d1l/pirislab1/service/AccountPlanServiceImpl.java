package com.d1l.pirislab1.service;

import com.d1l.pirislab1.dao.AccountPlanDao;
import com.d1l.pirislab1.model.AccountPlan;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AccountPlanServiceImpl implements AccountPlanService {

    private AccountPlanDao accountPlanDao;

    public void setAccountPlanDao(AccountPlanDao accountPlanDao) {
        this.accountPlanDao = accountPlanDao;
    }

    @Transactional
    public void addAccountPlan(AccountPlan accountPlan) {
        this.accountPlanDao.addAccountPlan(accountPlan);
    }

    @Transactional
    public void updateAccountPlan(AccountPlan accountPlan) {
        this.accountPlanDao.updateAccountPlan(accountPlan);
    }

    @Transactional
    public List<AccountPlan> listAccountPlans() {
        return this.accountPlanDao.listAccountPlans();
    }

    @Transactional
    public AccountPlan getAccountPlanById(long id) {
        return this.accountPlanDao.getAccountPlanById(id);
    }

    @Transactional
    public void removeAccountPlan(int id) {
        this.accountPlanDao.removeAccountPlan(id);
    }
    
}
