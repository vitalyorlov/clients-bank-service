package com.d1l.pirislab1.service;

import com.d1l.pirislab1.model.City;

import java.util.List;

public interface CityService {
    public void addCity(City c);
    public void updateCity(City c);
    public List<City> listCities();
    public City getCityById(int id);
    public void removeCity(int id);
}
