package com.d1l.pirislab1.service;

import com.d1l.pirislab1.model.Account;

import java.util.List;

public interface AccountService {
    public void addAccount(Account account);
    public void updateAccount(Account account);
    public List<Account> listAccounts();
    public Account getAccountById(int id);
    public void removeAccount(int id);
    public Account getAccountByDepositId(int depositId);
    public Account getMainAccountByDepositId(int depositId);
    public Account getAccountByCreditId(int creditId);
    public Account getMainAccountByCreditId(int id);
    public List<Account> getDepositAccountsByClientId(int clientId);
}
