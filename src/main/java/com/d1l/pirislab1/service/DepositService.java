package com.d1l.pirislab1.service;

import com.d1l.pirislab1.model.Deposit;

import java.util.List;

public interface DepositService {
    public void addDeposit(Deposit c);
    public void updateDeposit(Deposit c);
    public List<Deposit> listDeposits();
    public Deposit getDepositById(int id);
    public void removeDeposit(int id);
}
