package com.d1l.pirislab1.service;

import com.d1l.pirislab1.dao.CardDao;
import com.d1l.pirislab1.model.Card;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CardServiceImpl implements CardService {

    private CardDao cardDao;

    public void setCardDao(CardDao cardDao) {
        this.cardDao = cardDao;
    }

    @Transactional
    public void addCard(Card c) {
        this.cardDao.addCard(c);
    }

    @Transactional
    public void updateCard(Card c) {
        this.cardDao.updateCard(c);
    }

    @Transactional
    public List<Card> listCards() {
        return this.cardDao.listCards();
    }

    @Transactional
    public Card getCardById(int id) {
        return this.cardDao.getCardById(id);
    }

    @Transactional
    public void removeCard(int id) {
        this.cardDao.removeCard(id);
    }

    @Transactional
    public Card getCardByNumberAndPin(String number, String pin) { return this.cardDao.getCardByNumberAndPin(number, pin);}
    
}
