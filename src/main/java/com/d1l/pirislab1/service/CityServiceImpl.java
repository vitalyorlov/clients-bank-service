package com.d1l.pirislab1.service;

import com.d1l.pirislab1.dao.CityDao;
import com.d1l.pirislab1.model.City;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    private CityDao cityDao;

    public void setCityDao(CityDao cityDao) {
        this.cityDao = cityDao;
    }

    @Transactional
    public void addCity(City c) {
        this.cityDao.addCity(c);
    }

    @Transactional
    public void updateCity(City c) {
        this.cityDao.updateCity(c);
    }

    @Transactional
    public List<City> listCities() {
        return this.cityDao.listCities();
    }

    @Transactional
    public City getCityById(int id) {
        return this.cityDao.getCityById(id);
    }

    @Transactional
    public void removeCity(int id) {
        this.cityDao.removeCity(id);
    }

}
