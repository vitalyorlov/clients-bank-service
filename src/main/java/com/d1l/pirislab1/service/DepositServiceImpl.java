package com.d1l.pirislab1.service;

import com.d1l.pirislab1.dao.DepositDao;
import com.d1l.pirislab1.model.Deposit;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DepositServiceImpl implements DepositService {

    private DepositDao depositDao;

    public void setDepositDao(DepositDao depositDao) {
        this.depositDao = depositDao;
    }

    @Transactional
    public void addDeposit(Deposit deposit) {
        this.depositDao.addDeposit(deposit);
    }

    @Transactional
    public void updateDeposit(Deposit deposit) {
        this.depositDao.updateDeposit(deposit);
    }

    @Transactional
    public List<Deposit> listDeposits() {
        return this.depositDao.listDeposits();
    }

    @Transactional
    public Deposit getDepositById(int id) {
        return this.depositDao.getDepositById(id);
    }

    @Transactional
    public void removeDeposit(int id) {
        this.depositDao.removeDeposit(id);
    }
    
}
