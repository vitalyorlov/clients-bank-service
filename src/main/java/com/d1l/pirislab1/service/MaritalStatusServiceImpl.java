package com.d1l.pirislab1.service;

import com.d1l.pirislab1.dao.MaritalStatusDao;
import com.d1l.pirislab1.model.MaritalStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MaritalStatusServiceImpl implements MaritalStatusService {

    private MaritalStatusDao maritalStatusDao;

    public void setMaritalStatusDao(MaritalStatusDao maritalStatusDao) {
        this.maritalStatusDao = maritalStatusDao;
    }

    @Transactional
    public void addMaritalStatus(MaritalStatus ms) {
        this.maritalStatusDao.addMaritalStatus(ms);
    }

    @Transactional
    public void updateMaritalStatus(MaritalStatus ms) {
        this.maritalStatusDao.updateMaritalStatus(ms);
    }

    @Transactional
    public List<MaritalStatus> listMaritalStatuses() {
        return this.maritalStatusDao.listMaritalStatuses();
    }

    @Transactional
    public MaritalStatus getMaritalStatusById(int id) {
        return this.maritalStatusDao.getMaritalStatusById(id);
    }

    @Transactional
    public void removeMaritalStatus(int id) {
        this.maritalStatusDao.removeMaritalStatus(id);
    }

}
