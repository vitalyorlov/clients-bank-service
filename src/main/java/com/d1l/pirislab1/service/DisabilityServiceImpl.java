package com.d1l.pirislab1.service;

import com.d1l.pirislab1.dao.DisabilityDao;
import com.d1l.pirislab1.model.Disability;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DisabilityServiceImpl implements DisabilityService {

    private DisabilityDao disabilityDao;

    public void setDisabilityDao(DisabilityDao disabilityDao) {
        this.disabilityDao = disabilityDao;
    }

    @Transactional
    public void addDisability(Disability d) {
        this.disabilityDao.addDisability(d);
    }

    @Transactional
    public void updateDisability(Disability d) {
        this.disabilityDao.updateDisability(d);
    }

    @Transactional
    public List<Disability> listDisabilities() {
        return this.disabilityDao.listDisabilities();
    }

    @Transactional
    public Disability getDisabilityById(int id) {
        return this.disabilityDao.getDisabilityById(id);
    }

    @Transactional
    public void removeDisability(int id) {
        this.disabilityDao.removeDisability(id);
    }

}
