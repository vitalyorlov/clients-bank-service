package com.d1l.pirislab1.service;

import com.d1l.pirislab1.dao.CurrencyDao;
import com.d1l.pirislab1.dao.CurrencyDao;
import com.d1l.pirislab1.model.Currency;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CurrencyServiceImpl implements CurrencyService {

    private CurrencyDao currencyDao;

    public void setCurrencyDao(CurrencyDao currencyDao) {
        this.currencyDao = currencyDao;
    }

    @Transactional
    public void addCurrency(Currency c) {
        this.currencyDao.addCurrency(c);
    }

    @Transactional
    public void updateCurrency(Currency c) {
        this.currencyDao.updateCurrency(c);
    }

    @Transactional
    public List<Currency> listCurrencies() {
        return this.currencyDao.listCurrencies();
    }

    @Transactional
    public Currency getCurrencyById(int id) {
        return this.currencyDao.getCurrencyById(id);
    }

    @Transactional
    public void removeCurrency(int id) {
        this.currencyDao.removeCurrency(id);
    }
    
}
