package com.d1l.pirislab1.service;

import com.d1l.pirislab1.model.MaritalStatus;

import java.util.List;

public interface MaritalStatusService {
    public void addMaritalStatus(MaritalStatus c);
    public void updateMaritalStatus(MaritalStatus c);
    public List<MaritalStatus> listMaritalStatuses();
    public MaritalStatus getMaritalStatusById(int id);
    public void removeMaritalStatus(int id);
}
