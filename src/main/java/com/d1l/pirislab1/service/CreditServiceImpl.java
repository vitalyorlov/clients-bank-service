package com.d1l.pirislab1.service;

import com.d1l.pirislab1.dao.CreditDao;
import com.d1l.pirislab1.model.Credit;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CreditServiceImpl implements CreditService {

    private CreditDao creditDao;

    public void setCreditDao(CreditDao creditDao) {
        this.creditDao = creditDao;
    }

    @Transactional
    public void addCredit(Credit c) {
        this.creditDao.addCredit(c);
    }

    @Transactional
    public void updateCredit(Credit c) {
        this.creditDao.updateCredit(c);
    }

    @Transactional
    public List<Credit> listCredits() {
        return this.creditDao.listCredits();
    }

    @Transactional
    public Credit getCreditById(int id) {
        return this.creditDao.getCreditById(id);
    }

    @Transactional
    public void removeCredit(int id) {
        this.creditDao.removeCredit(id);
    }
    
}
