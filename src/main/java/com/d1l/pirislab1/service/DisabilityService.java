package com.d1l.pirislab1.service;

import com.d1l.pirislab1.model.Disability;

import java.util.List;

public interface DisabilityService {
    public void addDisability(Disability c);
    public void updateDisability(Disability c);
    public List<Disability> listDisabilities();
    public Disability getDisabilityById(int id);
    public void removeDisability(int id);
}
