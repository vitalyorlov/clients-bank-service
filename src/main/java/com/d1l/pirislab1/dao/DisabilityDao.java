package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.Disability;

import java.util.List;

public interface DisabilityDao {
    public void addDisability(Disability d);
    public void updateDisability(Disability d);
    public List<Disability> listDisabilities();
    public Disability getDisabilityById(int id);
    public void removeDisability(int id);
}
