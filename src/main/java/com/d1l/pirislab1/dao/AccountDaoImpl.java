package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.Account;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class AccountDaoImpl implements AccountDao {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

    public void addAccount(Account account) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(account);
    }

    public void updateAccount(Account account) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(account);
    }

    @SuppressWarnings("unchecked")
    public List<Account> listAccounts() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Account> accountsList = (List<Account>) session.createQuery("from Account").list();
        return accountsList;
    }

    public Account getAccountById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Account c = (Account) session.createQuery("from Account where id=" + id).uniqueResult();
        return c;
    }

    public void removeAccount(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Account c = (Account) session.load(Account.class, new Long(id));
        if(null != c){
            session.delete(c);
        }
    }

    public Account getAccountByDepositId(int depositId) {
        Session session = this.sessionFactory.getCurrentSession();
        Account account = (Account) session.createQuery("from Account where deposit.id=" + depositId
                + "and typeAccount='percentage'").uniqueResult();
                //.add(Restrictions.eq("deposit.id", Long.valueOf(depositId))).list();
                //.add(Restrictions.eq("typeAccount", "percentage"));
//        for (Account account : accounts)
//            if (account.getTypeAccount().equals("percentage"))
//                return account;
        return account;
    }

    public Account getMainAccountByDepositId(int depositId) {
        Session session = this.sessionFactory.getCurrentSession();
        Account account = (Account) session.createQuery("from Account where deposit.id=" + depositId
                + "and typeAccount='main'").uniqueResult();
        return account;
    }


    public Account getAccountByCreditId(int creditId) {
        Session session = this.sessionFactory.getCurrentSession();
        Account account = (Account) session.createQuery("from Account where cred.id=" + creditId
                + "and typeAccount='percentage'").uniqueResult();
        return account;
    }

    public Account getMainAccountByCreditId(int creditId) {
        Session session = this.sessionFactory.getCurrentSession();
        Account account = (Account) session.createQuery("from Account where cred.id=" + creditId
                + "and typeAccount='main'").uniqueResult();
        return account;
    }

    public List<Account> getDepositAccountsByClientId(int clientId) {
        Session session = this.sessionFactory.getCurrentSession();
        List<Account> accounts = (List<Account>) session.createQuery("from Account where client.id=" + clientId
                + "and cred=NULL").list();
        return accounts;
    }
}
