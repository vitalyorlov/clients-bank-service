package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.Credit;

import java.util.List;

public interface CreditDao {
    public void addCredit(Credit c);
    public void updateCredit(Credit c);
    public List<Credit> listCredits();
    public Credit getCreditById(int id);
    public void removeCredit(int id);
}
