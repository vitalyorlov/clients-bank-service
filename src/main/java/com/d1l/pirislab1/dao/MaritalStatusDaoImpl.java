package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.MaritalStatus;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MaritalStatusDaoImpl implements MaritalStatusDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

    public void addMaritalStatus(MaritalStatus maritalStatus) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(maritalStatus);
    }

    public void updateMaritalStatus(MaritalStatus p) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(p);
    }

    @SuppressWarnings("unchecked")
    public List<MaritalStatus> listMaritalStatuses() {
        Session session = this.sessionFactory.getCurrentSession();
        List<MaritalStatus> maritalStatusesList = session.createQuery("from MaritalStatus").list();
        return maritalStatusesList;
    }

    public MaritalStatus getMaritalStatusById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        MaritalStatus c = (MaritalStatus) session.load(MaritalStatus.class, new Integer(id));
        return c;
    }

    public void removeMaritalStatus(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        MaritalStatus c = (MaritalStatus) session.load(MaritalStatus.class, new Integer(id));
        if(null != c){
            session.delete(c);
        }
    }

}
