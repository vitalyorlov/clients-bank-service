package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.Deposit;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class DepositDaoImpl implements DepositDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

    public void addDeposit(Deposit deposit) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(deposit);
    }

    public void updateDeposit(Deposit deposit) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(deposit);
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public List<Deposit> listDeposits() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Deposit> depositsList = (List<Deposit>) session.createCriteria(Deposit.class).list();
        return depositsList;
    }

    public Deposit getDepositById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Deposit deposit = (Deposit) session.get(Deposit.class, new Long(id));
        return deposit;
    }

    public void removeDeposit(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Deposit deposit = (Deposit) session.get(Deposit.class, new Long(id));
        if(null != deposit){
            session.delete(deposit);
        }
    }


}
