package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.Credit;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class CreditDaoImpl implements CreditDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

    public void addCredit(Credit credit) {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(credit);
    }

    public void updateCredit(Credit credit) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(credit);
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public List<Credit> listCredits() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Credit> creditsList = (List<Credit>) session.createCriteria(Credit.class).list();
        return creditsList;
    }

    public Credit getCreditById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Credit c = (Credit) session.get(Credit.class, new Long(id));
        return c;
    }

    public void removeCredit(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Credit c = (Credit) session.get(Credit.class, new Long(id));
        if(null != c){
            session.delete(c);
        }
    }


}
