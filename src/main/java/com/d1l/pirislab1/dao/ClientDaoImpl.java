package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.Client;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ClientDaoImpl implements ClientDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

    public void addClient(Client client) {
        Session session = this.sessionFactory.getCurrentSession();
        session.save(client);
    }

    public void updateClient(Client p) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(p);
    }

    @SuppressWarnings("unchecked")
    public List<Client> listClients() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Client> clientsList = session.createQuery("from Client").list();
        return clientsList;
    }

    public Client getClientById(long id) {
        Session session = this.sessionFactory.getCurrentSession();
        Client c = (Client) session.get(Client.class, new Long(id));
        return c;
    }

    public void removeClient(long id) {
        Session session = this.sessionFactory.getCurrentSession();
        Client c = (Client) session.load(Client.class, new Long(id));
        if(null != c){
            session.delete(c);
        }
    }

    @Transactional
    public Client getClientByFullname(String firstname, String lastname, String middlename) {
        Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Client.class);
        criteria.add(Restrictions.eq("firstname", firstname));
        criteria.add(Restrictions.eq("lastname", lastname));
        criteria.add(Restrictions.eq("middlename", middlename));
        Client c = (Client) criteria.uniqueResult();
        return c;
    }

    @Transactional
    public Client getClientByPassport(String passportSeries, String passportNumber) {
        Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Client.class);
        criteria.add(Restrictions.eq("passportSeries", passportSeries));
        criteria.add(Restrictions.eq("passportNumber", passportNumber));
        Client c = (Client) criteria.uniqueResult();
        return c;
    }

    @Transactional
    public Client getClientByIdentityNumber(String identityNumber) {
        Session session = this.sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Client.class);
        criteria.add(Restrictions.eq("identityNumber", identityNumber));
        Client c = (Client) criteria.uniqueResult();
        return c;
    }
}
