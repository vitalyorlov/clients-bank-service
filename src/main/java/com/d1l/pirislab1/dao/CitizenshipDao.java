package com.d1l.pirislab1.dao;


import com.d1l.pirislab1.model.Citizenship;

import java.util.List;

public interface CitizenshipDao {
    public void addCitizenship(Citizenship c);
    public void updateCitizenship(Citizenship c);
    public List<Citizenship> listCitizenships();
    public Citizenship getCitizenshipById(int id);
    public void removeCitizenship(int id);
}
