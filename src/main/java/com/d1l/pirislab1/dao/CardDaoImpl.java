package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.Card;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CardDaoImpl implements CardDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

    public void addCard(Card card) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(card);
    }

    public void updateCard(Card card) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(card);
    }

    @SuppressWarnings("unchecked")
    public List<Card> listCards() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Card> cardsList = session.createQuery("from Card").list();
        return cardsList;
    }

    public Card getCardById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Card c = (Card) session.load(Card.class, new Long(id));
        return c;
    }

    public void removeCard(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Card c = (Card) session.load(Card.class, new Long(id));
        if(null != c){
            session.delete(c);
        }
    }

    public Card getCardByNumberAndPin(String number, String pin) {
        Session session = this.sessionFactory.getCurrentSession();
        Card card = (Card) session.createQuery("from Card where numberCard=" + number
                + "and pin=" + pin).uniqueResult();
        return card;
    }
}
