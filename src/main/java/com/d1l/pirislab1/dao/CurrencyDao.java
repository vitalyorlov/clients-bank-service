package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.Currency;

import java.util.List;

public interface CurrencyDao {
    public void addCurrency(Currency c);
    public void updateCurrency(Currency c);
    public List<Currency> listCurrencies();
    public Currency getCurrencyById(int id);
    public void removeCurrency(int id);
}
