package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.AccountPlan;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountPlanDaoImpl implements AccountPlanDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

    public void addAccountPlan(AccountPlan accountPlan) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(accountPlan);
    }

    public void updateAccountPlan(AccountPlan accountPlan) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(accountPlan);
    }

    @SuppressWarnings("unchecked")
    public List<AccountPlan> listAccountPlans() {
        Session session = this.sessionFactory.getCurrentSession();
        List<AccountPlan> accountPlansList = session.createQuery("from AccountPlan").list();
        return accountPlansList;
    }

    public AccountPlan getAccountPlanById(long id) {
        Session session = this.sessionFactory.getCurrentSession();
        AccountPlan accountPlan = (AccountPlan) session.get(AccountPlan.class, new Long(id));
        return accountPlan;
    }

    public void removeAccountPlan(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        AccountPlan accountPlan = (AccountPlan) session.get(AccountPlan.class, new Long(id));
        if(null != accountPlan){
            session.delete(accountPlan);
        }
    }


}
