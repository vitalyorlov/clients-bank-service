package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.Disability;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DisabilityDaoImpl implements DisabilityDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

    public void addDisability(Disability disability) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(disability);
    }

    public void updateDisability(Disability p) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(p);
    }

    @SuppressWarnings("unchecked")
    public List<Disability> listDisabilities() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Disability> disabilitiesList = session.createQuery("from Disability").list();
        return disabilitiesList;
    }

    public Disability getDisabilityById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Disability c = (Disability) session.load(Disability.class, new Integer(id));
        return c;
    }

    public void removeDisability(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Disability c = (Disability) session.load(Disability.class, new Integer(id));
        if(null != c){
            session.delete(c);
        }
    }

}
