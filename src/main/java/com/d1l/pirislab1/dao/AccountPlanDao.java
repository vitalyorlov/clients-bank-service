package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.AccountPlan;

import java.util.List;

public interface AccountPlanDao {
    public void addAccountPlan(AccountPlan accountPlan);
    public void updateAccountPlan(AccountPlan accountPlan);
    public List<AccountPlan> listAccountPlans();
    public AccountPlan getAccountPlanById(long id);
    public void removeAccountPlan(int id);
}
