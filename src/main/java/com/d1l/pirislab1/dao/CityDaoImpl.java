package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.City;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CityDaoImpl implements CityDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

    public void addCity(City city) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(city);
    }

    public void updateCity(City city) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(city);
    }

    @SuppressWarnings("unchecked")
    public List<City> listCities() {
        Session session = this.sessionFactory.getCurrentSession();
        List<City> citiesList = session.createQuery("from City").list();
        return citiesList;
    }

    public City getCityById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        City c = (City) session.load(City.class, new Integer(id));
        return c;
    }

    public void removeCity(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        City c = (City) session.load(City.class, new Integer(id));
        if(null != c){
            session.delete(c);
        }
    }

}
