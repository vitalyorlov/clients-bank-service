package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.Card;

import java.util.List;

public interface CardDao {
    public void addCard(Card c);
    public void updateCard(Card c);
    public List<Card> listCards();
    public Card getCardById(int id);
    public void removeCard(int id);
    public Card getCardByNumberAndPin(String number, String pin);
}
