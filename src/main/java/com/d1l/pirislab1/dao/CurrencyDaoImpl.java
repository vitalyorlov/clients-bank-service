package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.Currency;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CurrencyDaoImpl implements CurrencyDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

    public void addCurrency(Currency currency) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(currency);
    }

    public void updateCurrency(Currency currency) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(currency);
    }

    @SuppressWarnings("unchecked")
    public List<Currency> listCurrencies() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Currency> currenciesList = session.createQuery("from Currency").list();
        return currenciesList;
    }

    public Currency getCurrencyById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Currency c = (Currency) session.load(Currency.class, new Integer(id));
        return c;
    }

    public void removeCurrency(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Currency c = (Currency) session.load(Currency.class, new Integer(id));
        if(null != c){
            session.delete(c);
        }
    }


}
