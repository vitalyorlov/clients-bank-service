package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.Account;

import java.util.List;

public interface AccountDao {
    public void addAccount(Account account);
    public void updateAccount(Account account);
    public List<Account> listAccounts();
    public Account getAccountById(int id);
    public void removeAccount(int id);
    public Account getAccountByDepositId(int depositId);
    public Account getMainAccountByDepositId(int depositId);
    public Account getAccountByCreditId(int creditId);
    public Account getMainAccountByCreditId(int creditId);
    public List<Account> getDepositAccountsByClientId(int clientId);
}
