package com.d1l.pirislab1.dao;

import com.d1l.pirislab1.model.Citizenship;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CitizenshipDaoImpl implements CitizenshipDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

    public void addCitizenship(Citizenship citizenship) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(citizenship);
    }

    public void updateCitizenship(Citizenship p) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(p);
    }

    @SuppressWarnings("unchecked")
    public List<Citizenship> listCitizenships() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Citizenship> citizenshipsList = session.createQuery("from Citizenship").list();
        return citizenshipsList;
    }

    public Citizenship getCitizenshipById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Citizenship c = (Citizenship) session.load(Citizenship.class, new Integer(id));
        return c;
    }

    public void removeCitizenship(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Citizenship c = (Citizenship) session.load(Citizenship.class, new Integer(id));
        if(null != c){
            session.delete(c);
        }
    }

}
