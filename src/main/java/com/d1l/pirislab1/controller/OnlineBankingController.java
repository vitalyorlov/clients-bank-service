package com.d1l.pirislab1.controller;

import com.d1l.pirislab1.model.Account;
import com.d1l.pirislab1.model.Card;
import com.d1l.pirislab1.service.AccountService;
import com.d1l.pirislab1.service.CardService;
import com.d1l.pirislab1.util.PrintHelper;
import com.oracle.webservices.internal.api.message.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

@Controller
public class OnlineBankingController {

    private CardService cardService;
    private AccountService accountService;

    @Autowired(required = true)
    @Qualifier(value = "cardService")
    public void setCreditService(CardService cs){
        this.cardService = cs;
    }

    @Autowired(required = true)
    @Qualifier(value = "accountService")
    public void setAccountService(AccountService as){
        this.accountService = as;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String getLoginPage(Model model, @ModelAttribute("errorMessage") String errorMessage) {
        if (errorMessage != null)
            model.addAttribute("errorMessage", errorMessage);
        return "login";
    }

    @RequestMapping(value = "/authorizate", method = RequestMethod.POST)
    public String authorizate(HttpServletRequest request, @RequestParam("numberCard") String numberCard,
                              @RequestParam("pinCode") String pinCode, RedirectAttributes redirectAttributes) {
        try {
            int attemptCount = (Integer) request.getSession().getAttribute("attemptCount");
        } catch (Exception ex) { request.getSession().setAttribute("attemptCount", 0); }

        if((Integer)request.getSession().getAttribute("attemptCount") < 3) {
            Card card = cardService.getCardByNumberAndPin(numberCard, pinCode);
            if (card != null) {
                request.getSession().setAttribute("cardId", card.getId());
                request.getSession().setAttribute("clientId", card.getClientId());
                request.getSession().setAttribute("creditId", card.getCreditId());
                request.getSession().setAttribute("attemptCount", 0);
                return "personal-account";
            }
            else {
                redirectAttributes.addFlashAttribute("errorMessage", "NumberCard or PIN-Code is incorrect");
                request.getSession().setAttribute("attemptCount",
                        (Integer)request.getSession().getAttribute("attemptCount") + 1);
                return "redirect:/login";
            }
        }
        request.getSession().setAttribute("attemptCount", 0);
        return "redirect:/";
    }

    @RequestMapping(value = "/personal-account", method = RequestMethod.GET)
    public String getPersonalAccountPage(Model model, @ModelAttribute("errorMessage") String errorMessage,
                                         @ModelAttribute("successMessage") String successMessage) {
        if (errorMessage != null)
            model.addAttribute("errorMessage", errorMessage);
        if (successMessage != null)
            model.addAttribute("successMessage", successMessage);
        return "personal-account";
    }

    @RequestMapping(value = "/get-money", method = RequestMethod.GET)
    public String getMoneyPage(Model model) {
        return "get-money-page";
    }

    @RequestMapping(value = "/get-credit-account", method = RequestMethod.GET)
    public String getCreditAccountPage(HttpServletRequest request, Model model) {
        Account account = accountService.getMainAccountByCreditId((Integer)request.getSession().getAttribute("creditId"));
        model.addAttribute("account", account);
        return "get-credit-account-page";
    }

    @RequestMapping(value = "/get-deposit-accounts", method = RequestMethod.GET)
    public String getDepositAccountsPage(HttpServletRequest request, Model model) {
        List<Account> accounts = accountService.getDepositAccountsByClientId((Integer)request.getSession().getAttribute("clientId"));
        model.addAttribute("listAccounts", accounts);
        return "get-deposit-accounts-page";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request) {
        request.getSession().invalidate();
        return "redirect:/";
    }

    @RequestMapping(value = "/give-me-money", method = RequestMethod.POST)
    public String giveMeMoney(HttpServletRequest request, @RequestParam("sum") String sum, HttpServletResponse response, RedirectAttributes redirectAttributes) {
        Account account = accountService.getMainAccountByCreditId((Integer) request.getSession().getAttribute("creditId"));
        String balance = account.getBalance();
        if (Double.valueOf(balance) < Double.valueOf(sum)) {
            redirectAttributes.addFlashAttribute("errorMessage", "Insufficient funds in the account");
        } else {
            redirectAttributes.addFlashAttribute("successMessage", "Operation completed succesfully");
            account.setBalance(String.valueOf(Double.valueOf(account.getBalance()) - Double.valueOf(sum)));
            accountService.updateAccount(account);

            ByteArrayOutputStream baos = PrintHelper.generateGetMoneyReport(account, Double.valueOf(sum));

            try {
                makeResponse(baos, "application/pdf", "report.pdf", response);
            } catch (IOException ex) { }
        }

        return "redirect:/personal-account";
    }

    @RequestMapping(value = "/print-account", method = RequestMethod.GET)
    public String printCreditAccount(Model model, @RequestParam("id") int id, HttpServletResponse response) {

        ByteArrayOutputStream baos = PrintHelper.generateAccountInfoById(accountService.getAccountById(id));

        try {
            makeResponse(baos, "application/pdf", "report.pdf", response);
        } catch (IOException ex) { }

        return "redirect:/personal-account";
    }

    private void makeResponse(ByteArrayOutputStream stream, String contentType, String fileName,
                              HttpServletResponse response) throws IOException {
        response.setContentType(contentType);
        response.setHeader("Content-Disposition",
                "inline; filename=" + fileName);
        response.setContentLength(stream.size());

        OutputStream os = response.getOutputStream();
        os.write(stream.toByteArray());
        os.flush();
        os.close();
        stream.reset();
    }

}
