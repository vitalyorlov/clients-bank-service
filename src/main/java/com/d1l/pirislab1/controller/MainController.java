package com.d1l.pirislab1.controller;

import com.d1l.pirislab1.model.*;
import com.d1l.pirislab1.service.*;
import com.d1l.pirislab1.validator.ClientValidator;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class MainController {

    private ClientService clientService;
    private CitizenshipService citizenshipService;
    private CityService cityService;
    private DisabilityService disabilityService;
    private MaritalStatusService maritalStatusService;

    @Autowired(required = true)
    @Qualifier(value = "clientService")
    public void setClientService(ClientService cs){
        this.clientService = cs;
    }

    @Autowired(required = true)
    @Qualifier(value = "citizenshipService")
    public void setCitizenshipService(CitizenshipService cs){
        this.citizenshipService = cs;
    }

    @Autowired(required = true)
    @Qualifier(value = "cityService")
    public void setCityService(CityService cs){
        this.cityService = cs;
    }

    @Autowired(required = true)
    @Qualifier(value = "disabilityService")
    public void setDisabilityService(DisabilityService ds){
        this.disabilityService = ds;
    }

    @Autowired(required = true)
    @Qualifier(value = "maritalStatusService")
    public void setMaritalStatusService(MaritalStatusService mss){
        this.maritalStatusService = mss;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String listClients(Model model, @ModelAttribute("errorMessage") String errorMessage) {
        getData(model);
        if (errorMessage != null)
            model.addAttribute("errorMessage", errorMessage);
        return "index";
    }

    private Model getData(Model model) {
        model.addAttribute("listClients", this.clientService.listClients());
        model.addAttribute("listCitizenships", this.citizenshipService.listCitizenships());
        model.addAttribute("listCities", this.cityService.listCities());
        model.addAttribute("listDisabilities", this.disabilityService.listDisabilities());
        model.addAttribute("listMaritalStatuses", this.maritalStatusService.listMaritalStatuses());
        model.addAttribute("client", new Client());

        return model;
    }

    //For add and update person both
    @RequestMapping(value= "/client/add", method = RequestMethod.POST)
    public String addClient(@ModelAttribute("client") Client c, Model model, RedirectAttributes redirectAttributes) {
        StringBuilder errorMessage = new StringBuilder();

        if (ClientValidator.validate(c, errorMessage)) {
            if (this.clientService.getClientByFullname(c.getFirstname(), c.getLastname(), c.getMiddlename()) != null) {
                redirectAttributes.addFlashAttribute("errorMessage", "Client with this fullname is exist");
                return "redirect:/";
            }
            if (this.clientService.getClientByPassport(c.getPassportSeries(), c.getPassportNumber()) != null) {
                redirectAttributes.addFlashAttribute("errorMessage", "Client with this passport is exist");
                return "redirect:/";
            }
            if (this.clientService.getClientByIdentityNumber(c.getIdentityNumber()) != null) {
                redirectAttributes.addFlashAttribute("errorMessage", "Client with this identity number is exist");
                return "redirect:/";
            }
            if (c.getId() == 0) {
                //new person, add it
                this.clientService.addClient(c);
            } else {
                //existing person, call update
                this.clientService.updateClient(c);
            }
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", errorMessage);
            return "redirect:/";
        }

        return "redirect:/";
    }

    @RequestMapping("/client/remove/{id}")
    public String removeClient(@PathVariable("id") long id){
        this.clientService.removeClient(id);
        return "redirect:/";
    }

    @RequestMapping("/client/edit/{id}")
    public String editClient(@PathVariable("id") long id, @ModelAttribute("client") Client c, Model model, RedirectAttributes redirectAttributes){
        StringBuilder errorMessage = new StringBuilder();

        if (ClientValidator.validate(c, errorMessage)) {
            if (this.clientService.getClientByFullname(c.getFirstname(), c.getLastname(), c.getMiddlename()) != null
                    && this.clientService.getClientByFullname(c.getFirstname(), c.getLastname(), c.getMiddlename()).getId() != c.getId()) {
                redirectAttributes.addFlashAttribute("errorMessage", "Client with this fullname is exist");
                return "redirect:/";
            }
            if (this.clientService.getClientByPassport(c.getPassportSeries(), c.getPassportNumber()) != null
                    && this.clientService.getClientByPassport(c.getPassportSeries(), c.getPassportNumber()).getId() != c.getId()) {
                redirectAttributes.addFlashAttribute("errorMessage", "Client with this passport is exist");
                return "redirect:/";
            }
            if (this.clientService.getClientByIdentityNumber(c.getIdentityNumber()) != null
                    && this.clientService.getClientByIdentityNumber(c.getIdentityNumber()).getId() != c.getId()) {
                redirectAttributes.addFlashAttribute("errorMessage", "Client with this identity number is exist");
                return "redirect:/";
            }
            this.clientService.updateClient(c);
        } else {
            redirectAttributes.addFlashAttribute("errorMessage", errorMessage);
            return "redirect:/";
        }

        return "redirect:/";
    }

    @RequestMapping(value = "/get-client-info", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody String getClientInfo(@RequestParam Integer id) {
        Client client = this.clientService.getClientById(id);
        return getClientInJson(client).toString();
    }

    private JsonObject getClientInJson(Client client) {
        JsonObject response = new JsonObject();
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        response.addProperty("id", client.getId());
        response.addProperty("firstname", client.getFirstname());
        response.addProperty("lastname", client.getLastname());
        response.addProperty("middlename", client.getMiddlename());
        response.addProperty("birthday", formatter.format(client.getBirthday()));
        response.addProperty("birthplace", client.getBirthplace());
        response.addProperty("email", client.getEmail());
        response.addProperty("identityNumber", client.getIdentityNumber());
        response.addProperty("income", client.getIncome());
        response.addProperty("issuedBy", client.getIssuedBy());
        response.addProperty("livingAddress", client.getLivingAddress());
        response.addProperty("passportNumber", client.getPassportNumber());
        response.addProperty("passportSeries", client.getPassportSeries());
        response.addProperty("phoneNumberHome", client.getPhoneNumberHome());
        response.addProperty("phoneNumberMobile", client.getPhoneNumberMobile());
        response.addProperty("retired", client.getRetired());
        response.addProperty("thePost", client.getThePost());
        response.addProperty("workplace", client.getWorkplace());
        response.addProperty("citizenshipId", client.getCitizenship().getId());
        response.addProperty("disabilityId", client.getDisability().getId());
        response.addProperty("issuedDate", formatter.format(client.getIssuedDate()));
        response.addProperty("livingCityId", client.getLivingCity().getId());
        response.addProperty("residenceCityId", client.getResidenceCity().getId());
        response.addProperty("maritalStatusId", client.getMaritalStatus().getId());

        return response;
    }

}
