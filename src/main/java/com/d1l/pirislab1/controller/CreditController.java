package com.d1l.pirislab1.controller;

import com.d1l.pirislab1.model.Account;
import com.d1l.pirislab1.model.AccountPlan;
import com.d1l.pirislab1.model.Credit;
import com.d1l.pirislab1.service.*;
import com.d1l.pirislab1.util.CalcHelper;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Controller
public class CreditController {

    private CreditService creditService;
    private AccountService accountService;
    private AccountPlanService accountPlanService;
    private CurrencyService currencyService;
    private ClientService clientService;
    private DepositService depositService;

    @Autowired(required = true)
    @Qualifier(value = "creditService")
    public void setCreditService(CreditService cs){
        this.creditService = cs;
    }

    @Autowired(required = true)
    @Qualifier(value = "accountService")
    public void setAccountService(AccountService as){
        this.accountService = as;
    }

    @Autowired(required = true)
    @Qualifier(value = "accountPlanService")
    public void setAccountPlanService(AccountPlanService aps){
        this.accountPlanService = aps;
    }

    @Autowired(required = true)
    @Qualifier(value = "currencyService")
    public void setCurrencyService(CurrencyService cs){
        this.currencyService = cs;
    }

    @Autowired(required = true)
    @Qualifier(value = "clientService")
    public void setClientService(ClientService cs){
        this.clientService = cs;
    }

    @Autowired(required = true)
    @Qualifier(value = "depositService")
    public void setCityService(DepositService ds){
        this.depositService = ds;
    }

    @RequestMapping(value = "/create-credit", method = RequestMethod.GET)
    public String chooseCredit(Model model) {
        return "select-credit";
    }

    @RequestMapping(value = "/standard-credit", method = { RequestMethod.GET, RequestMethod.POST })
    public String createStandardCredit(Model model) {
        model.addAttribute("listAccountPlans", this.accountPlanService.listAccountPlans());
        model.addAttribute("listClients", this.clientService.listClients());
        model.addAttribute("listCurrencies", this.currencyService.listCurrencies());
        model.addAttribute("credit", new Credit());
        return "standard-credit";
    }

    @RequestMapping(value = "/standard-credit-new", method = RequestMethod.POST)
    public String newStandardCredit(Model model, @ModelAttribute("credit") Credit credit, @RequestParam(value="accountPlanID", required=false) String accountPlanID) {
        createCreditWithAccounts(credit, Long.valueOf(accountPlanID));
        return "forward:/credits";
    }

    @RequestMapping(value = "/annuity-credit", method = { RequestMethod.GET, RequestMethod.POST })
    public String createAnnuityCredit(Model model) {
        model.addAttribute("listAccountPlans", this.accountPlanService.listAccountPlans());
        model.addAttribute("listClients", this.clientService.listClients());
        model.addAttribute("listCurrencies", this.currencyService.listCurrencies());
        model.addAttribute("credit", new Credit());
        return "annuity-credit";
    }

    @RequestMapping(value = "/annuity-credit-new", method = RequestMethod.POST)
    public String newAnnuityCredit(Model model, @ModelAttribute("credit") Credit credit,
                                   @RequestParam(value="accountPlanID", required=false) String accountPlanID) {
        createCreditWithAccounts(credit, Long.valueOf(accountPlanID));
        return "forward:/credits";
    }

    @RequestMapping(value = "/credits", method = { RequestMethod.GET, RequestMethod.POST })
    public String getCredits(Model model) {
        List<Account> accounts = new ArrayList<Account>();
        for (Account account : this.accountService.listAccounts()) {
            if(account.getCred() != null) accounts.add(account);
        }
        model.addAttribute("listAccounts", accounts);
        return "credits";
    }

    @RequestMapping(value = "/close-day-for-credits", method = RequestMethod.GET)
    public String closeDayForCredits(Model model) {
        closeDay();
        return "forward:/index";
    }

    @RequestMapping(value = "/info-credit", method = RequestMethod.GET)
    public String getInfo(Model model, @PathVariable("id") int id) {
        model.addAttribute("account", accountService.getAccountById(id));
        return "info-credit";
    }


    private void createCreditWithAccounts(Credit credit, long planId) {
        AccountPlan accountPlan = accountPlanService.getAccountPlanById(planId);
        String accountNumber = CalcHelper.createAccount(13, accountPlan.getCodePlan());
        double debet = CalcHelper.calculateDebetAmmount(accountPlan.getActivity(), Double.valueOf(credit.getSum()));
        double cred = CalcHelper.calculateCreditAmmount(accountPlan.getActivity(), Double.valueOf(credit.getSum()));

        credit.setClient(clientService.getClientById(credit.getClient().getId()));
        creditService.addCredit(credit);

        Account accountDev = accountService.listAccounts().get(0);//shitcode
        accountDev.setBalance(String.valueOf(Double.valueOf(accountDev.getBalance()) - Double.valueOf(credit.getSum())));
        accountService.updateAccount(accountDev);

        Account accountMain = new Account();
        accountMain.setActivityPlan("");
        accountMain.setBalance(credit.getSum());
        accountMain.setCodePlan(accountPlan.getCodePlan());
        accountMain.setNumberAccount(accountNumber);
        accountMain.setTypeAccount("main");
        accountMain.setDebet(String.valueOf(debet));
        accountMain.setCredit(String.valueOf(cred));
        accountMain.setCred(credit);
        accountMain.setClient(credit.getClient());
        accountService.addAccount(accountMain);

        Account accountPercentage = new Account();
        accountPercentage.setActivityPlan("");
        accountPercentage.setBalance("0");
        accountPercentage.setCodePlan(accountPlan.getCodePlan());
        accountPercentage.setNumberAccount(accountNumber);
        accountPercentage.setTypeAccount("percentage");
        accountPercentage.setDebet(String.valueOf(debet));
        accountPercentage.setCredit(String.valueOf(cred));
        accountPercentage.setCred(credit);
        accountPercentage.setClient(credit.getClient());

        calculatePercents(accountPercentage);
    }

    public void calculatePercents(Account account) {
        if (account.getCred().getType().equals("standard")) {

            double sumOfPercent = 0;
            for (int monthNumber = 1; monthNumber <= 12; monthNumber++) {
                sumOfPercent += (Double.valueOf(account.getCred().getSum())
                        - (Double.valueOf(account.getCred().getSum()) / 12 * monthNumber))
                        * (Double.valueOf(account.getCred().getPercent())) / 1200;
            }

            account.setBalance(String.valueOf(Double.valueOf(account.getBalance()) + sumOfPercent));
            accountService.addAccount(account);
        }
        if (account.getCred().getType().equals("annuity")) {
            double sumOfPercent = Double.valueOf(account.getCred().getSum()) * Double.valueOf(account.getCred().getPercent())
                    / 100;
            account.setBalance(String.valueOf(Double.valueOf(account.getBalance()) + sumOfPercent));
            accountService.addAccount(account);
        }

    }

    public void closeDay() {
        List<Account> accounts = accountService.listAccounts();

        Account accountDev = accounts.get(0);

        for (Account account : accounts) {
            if (account.getCred() != null) {
                Account accountMain = accountService.getMainAccountByCreditId((int)account.getCred().getId());
                Account accountPercentage = accountService.getAccountByCreditId((int)account.getCred().getId());
                accountDev.setBalance(String.valueOf(Double.valueOf(accountDev.getBalance())
                        + Double.valueOf(accountMain.getBalance())
                        + Double.valueOf(accountPercentage.getBalance())));

                accountMain.setBalance("0");
                accountPercentage.setBalance("0");

                accountService.updateAccount(accountMain);
                accountService.updateAccount(accountPercentage);
            }
        }

        accountService.updateAccount(accountDev);

    }

}
