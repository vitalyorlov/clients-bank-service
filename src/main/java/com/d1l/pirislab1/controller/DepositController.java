package com.d1l.pirislab1.controller;

import com.d1l.pirislab1.model.Account;
import com.d1l.pirislab1.model.AccountPlan;
import com.d1l.pirislab1.model.Credit;
import com.d1l.pirislab1.model.Deposit;
import com.d1l.pirislab1.service.*;
import com.d1l.pirislab1.util.CalcHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class DepositController {

    private DepositService depositService;
    private AccountService accountService;
    private AccountPlanService accountPlanService;
    private CurrencyService currencyService;
    private ClientService clientService;
    private CreditService creditService;

    @Autowired(required = true)
    @Qualifier(value = "depositService")
    public void setCityService(DepositService ds){
        this.depositService = ds;
    }

    @Autowired(required = true)
    @Qualifier(value = "accountService")
    public void setAccountService(AccountService as){
        this.accountService = as;
    }

    @Autowired(required = true)
    @Qualifier(value = "accountPlanService")
    public void setAccountPlanService(AccountPlanService aps){
        this.accountPlanService = aps;
    }

    @Autowired(required = true)
    @Qualifier(value = "currencyService")
    public void setCurrencyService(CurrencyService cs){
        this.currencyService = cs;
    }

    @Autowired(required = true)
    @Qualifier(value = "clientService")
    public void setClientService(ClientService cs){
        this.clientService = cs;
    }

    @Autowired(required = true)
    @Qualifier(value = "creditService")
    public void setCreditService(CreditService cs){
        this.creditService = cs;
    }

    @RequestMapping(value = "/create-deposit", method = RequestMethod.GET)
    public String selectDeposit(Model model) {
        return "select-deposit";
    }

    @RequestMapping(value = "/term-deposit", method = { RequestMethod.GET, RequestMethod.POST })
    public String createTermDeposit(Model model) {
        model.addAttribute("listAccountPlans", this.accountPlanService.listAccountPlans());
        model.addAttribute("listClients", this.clientService.listClients());
        model.addAttribute("listCurrencies", this.currencyService.listCurrencies());
        model.addAttribute("deposit", new Deposit());
        return "term-deposit";
    }

    @RequestMapping(value = "/demand-deposit", method = { RequestMethod.GET, RequestMethod.POST })
    public String createDemandDeposit(Model model) {
        model.addAttribute("listAccountPlans", this.accountPlanService.listAccountPlans());
        model.addAttribute("listClients", this.clientService.listClients());
        model.addAttribute("listCurrencies", this.currencyService.listCurrencies());
        model.addAttribute("deposit", new Deposit());
        return "demand-deposit";
    }

    @RequestMapping(value = "/term-deposit-new", method = RequestMethod.POST)
    public String newTermDeposit(Model model, @ModelAttribute("deposit") Deposit deposit,
                                 @RequestParam(value="accountPlanID", required=false) String accountPlanID) {
        createDepositWithAccounts(deposit, Long.valueOf(accountPlanID));
        return "forward:/deposits";
    }

    @RequestMapping(value = "/demand-deposit-new", method = RequestMethod.POST)
    public String newDemandDeposit(Model model, @ModelAttribute("deposit") Deposit deposit,
                                   @RequestParam(value="accountPlanID", required=false) String accountPlanID) {
        deposit.setExtend(new Date()); //don't use it, I set it because I forgot to enable null in extend date
        createDepositWithAccounts(deposit, Long.valueOf(accountPlanID));
        return "forward:/deposits";
    }

    @RequestMapping(value = "/deposits", method = { RequestMethod.GET, RequestMethod.POST })
    public String getDeposits(Model model) {
        List<Account> accounts = new ArrayList<Account>();
        for (Account account : this.accountService.listAccounts()) {
            if(account.getDeposit() != null) accounts.add(account);
        }
        model.addAttribute("listAccounts", accounts);
        return "deposits";
    }

    @RequestMapping(value = "/close-day-for-deposits", method = RequestMethod.GET)
    public String closeDayForDeposits(Model model) {
        closeDay();
        return "forward:/";
    }

    @RequestMapping(value = "/info-deposit", method = RequestMethod.GET)
    public String getInfo(Model model, @PathVariable("id") int id) {
        model.addAttribute("account", accountService.getAccountById(id));
        return "info-deposit";
    }

    private void createDepositWithAccounts(Deposit deposit, long planId) {
        AccountPlan accountPlan = accountPlanService.getAccountPlanById(planId);
        String accountNumber = CalcHelper.createAccount(13, accountPlan.getCodePlan());
        double debet = CalcHelper.calculateDebetAmmount(accountPlan.getActivity(), Double.valueOf(deposit.getSum()));
        double cred = CalcHelper.calculateCreditAmmount(accountPlan.getActivity(), Double.valueOf(deposit.getSum()));

        deposit.setClient(clientService.getClientById(deposit.getClient().getId()));
        depositService.addDeposit(deposit);

        Account accountDev = accountService.listAccounts().get(0);//shitcode
        accountDev.setBalance(String.valueOf(Double.valueOf(accountDev.getBalance()) + Double.valueOf(deposit.getSum())));
        accountService.updateAccount(accountDev);

        Account accountMain = new Account();
        accountMain.setActivityPlan("");
        accountMain.setBalance(deposit.getSum());
        accountMain.setCodePlan(accountPlan.getCodePlan());
        accountMain.setNumberAccount(accountNumber);
        accountMain.setTypeAccount("main");
        accountMain.setDebet(String.valueOf(debet));
        accountMain.setCredit(String.valueOf(cred));
        //accountMain.setCred(creditService.getCreditById(1));
        accountMain.setDeposit(deposit);
        accountMain.setClient(deposit.getClient());
        accountService.addAccount(accountMain);

        Account accountPercentage = new Account();
        accountPercentage.setActivityPlan("");
        accountPercentage.setBalance("0");
        accountPercentage.setCodePlan(accountPlan.getCodePlan());
        accountPercentage.setNumberAccount(accountNumber);
        accountPercentage.setTypeAccount("percentage");
        accountPercentage.setDebet(String.valueOf(debet));
        accountPercentage.setCredit(String.valueOf(cred));
        //accountMain.setCred(creditService.getCreditById(1));
        accountPercentage.setDeposit(deposit);
        accountPercentage.setClient(deposit.getClient());
        accountService.addAccount(accountPercentage);
    }

    public void closeDay() {
        List<Deposit> deposits = depositService.listDeposits();

        Account accountDev = accountService.listAccounts().get(0);//shitcode

        for (Deposit deposit : deposits) {
            Account account = accountService.getAccountByDepositId((int)deposit.getId());

            accountDev.setBalance(String.valueOf(Double.valueOf(accountDev.getBalance()) - (Double.valueOf(deposit.getSum())
                    * Double.valueOf(deposit.getPercent()) / 36500)));
            account.setBalance(String.valueOf(Double.valueOf(account.getBalance()) + (Double.valueOf(deposit.getSum())
                    * Double.valueOf(deposit.getPercent()) / 36500)));

            accountService.updateAccount(account);
        }

        accountService.updateAccount(accountDev);

    }
}
