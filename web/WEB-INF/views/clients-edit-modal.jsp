<div class="modal clients_edit_modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">.
            <form:form id="edit_form" method="post" action="client/edit/" modelAttribute="client">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Update client info</h4>
                </div>
                <div class="modal-body">
                    <form:input id="clients_edit_id" path="id" type="hidden"/>
                    <div class="form-group">
                        <label>First name: </label>
                        <form:input pattern="^[A-Za-z]{1,100}$" id="clients_edit_firstname" type="text" required="required" class="form-control" path="firstname"/>
                    </div>
                    <div class="form-group">
                        <label>Last name: </label>
                        <form:input pattern="^[A-Za-z]{1,100}$" id="clients_edit_lastname" type="text" required="required" class="form-control" path="lastname"/>
                    </div>
                    <div class="form-group">
                        <label>Middle name: </label>
                        <form:input pattern="^[A-Za-z]{1,100}$" id="clients_edit_middlename" type="text" required="required" class="form-control" path="middlename"/>
                    </div>
                    <div class="form-group">
                        <label>Birth date(dd/mm/yyyy): </label>
                        <form:input pattern="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" id="clients_edit_birthdate" type="text" required="required" class="form-control datemask" path="birthday"/>
                    </div>
                    <div class="form-group">
                        <label>Passport series: </label>
                        <form:input pattern="^[A-Za-z]{1,100}$" id="clients_edit_passport_series" type="text" required="required" class="form-control" path="passportSeries"/>
                    </div>
                    <div class="form-group">
                        <label>Passport number: </label>
                        <form:input pattern="^[0-9]{1,100}$" id="clients_edit_passport_number" type="text" required="required" class="form-control passnumber" path="passportNumber"/>
                    </div>
                    <div class="form-group">
                        <label>Issued by: </label>
                        <form:input id="clients_edit_issued_by" type="text" required="required" class="form-control" path="issuedBy"/>
                    </div>
                    <div class="form-group">
                        <label>Issued date(dd/mm/yyyy): </label>
                        <form:input pattern="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" id="clients_edit_issued_date" type="text" required="required" class="form-control datemask" path="issuedDate"/>
                    </div>
                    <div class="form-group">
                        <label>Identity number: </label>
                        <form:input pattern="^[A-Za-z0-9]{1,100}$" id="clients_edit_identity_number" type="text" required="required" class="form-control identity" path="identityNumber"/>
                    </div>
                    <div class="form-group">
                        <label>Birth place: </label>
                        <form:input id="clients_edit_birth_place" type="text" required="required" class="form-control" path="birthplace"/>
                    </div>
                    <div class="form-group">
                        <label>Living city: </label>
                        <form:select id="clients_edit_living_city_id" required="required" class="form-control" path="livingCity.id">
                            <c:forEach items="${listCities}" var="city">
                                <form:option value="${city.id}">
                                    ${city.name}
                                </form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                    <div class="form-group">
                        <label>Living address: </label>
                        <form:input id="clients_edit_living_address" type="text" required="required" class="form-control" path="livingAddress"/>
                    </div>
                    <div class="form-group">
                        <label>Home phone number: </label>
                        <form:input pattern="^[0-9\-+\s]{0,100}$" id="clients_edit_home_phone_number" type="text" class="form-control homephone" path="phoneNumberHome"/>
                    </div>
                    <div class="form-group">
                        <label>Mobile phone number: </label>
                        <form:input pattern="^[0-9\-+\s]{0,100}$" id="clients_edit_mobile_phone_number" type="text" class="form-control mobilephone" path="phoneNumberMobile"/>
                    </div>
                    <div class="form-group">
                        <label>E-mail: </label>
                        <form:input id="clients_edit_email" type="email" class="form-control" path="email"/>
                    </div>
                    <div class="form-group">
                        <label>The post: </label>
                        <form:input pattern="^[A-Za-z\s]{0,100}$" id="clients_edit_the_post" type="text" class="form-control" path="thePost"/>
                    </div>
                    <div class="form-group">
                        <label>Retired: </label><br />
                        <form:radiobutton id="clients_edit_retired_true" path="retired" value="true"/> Yes<br>
                        <form:radiobutton id="clients_edit_retired_false" path="retired" value="false"/> No<br><br>
                    </div>
                    <div class="form-group">
                        <label>Work place: </label>
                        <form:input id="clients_edit_work_place" type="text" class="form-control" path="workplace"/>
                    </div>
                    <div class="form-group">
                        <label>Income: </label>
                        <form:input pattern="^[0-9\s\.\,]{1,100}$" id="clients_edit_income" type="text" class="form-control" path="income"/>
                    </div>
                    <div class="form-group">
                        <label>Residence city: </label>
                        <form:select id="clients_edit_residence_city_id" required="required" class="form-control" path="residenceCity.id">
                            <c:forEach items="${listCities}" var="city">
                                <form:option value="${city.id}">
                                    ${city.name}
                                </form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                    <div class="form-group">
                        <label>Citizenship: </label>
                        <form:select id="clients_edit_citizenship_id" required="required" class="form-control" path="citizenship.id">
                            <c:forEach items="${listCitizenships}" var="citizenship">
                                <form:option value="${citizenship.id}">
                                    ${citizenship.name}
                                </form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                    <div class="form-group">
                        <label>Marital status: </label>
                        <form:select id="clients_edit_marital_status_id" required="required" class="form-control" path="maritalStatus.id">
                            <c:forEach items="${listMaritalStatuses}" var="maritalStatus">
                                <form:option value="${maritalStatus.id}">
                                    ${maritalStatus.name}
                                </form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                    <div class="form-group">
                        <label>Disability: </label>
                        <form:select id="clients_edit_disability_id" required="required" class="form-control" path="disability.id">
                            <c:forEach items="${listDisabilities}" var="disability">
                                <form:option value="${disability.id}">
                                    ${disability.name}
                                </form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <form:button type="submit" class="btn btn-primary add-btn">Save changes</form:button>
                    </div>
                </div>
                </form:form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
