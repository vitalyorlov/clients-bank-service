<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Banking online</title>
    <link href="../../resources/css/bootstrap.css" rel="stylesheet" >
    <link href="../../resources/css/style.css" rel="stylesheet">
    <script src="../../resources/js/jquery-3.1.1.min.js"></script>
    <script src="../../resources/js/bootstrap.js"></script>
    <script src="../../resources/js/jquery.mask.js"></script>
</head>
<body>
<%@include file='menu.jsp'%>
<div class="container">
    <div class="starter-template">
        <h3 class="text-center" style="margin-bottom: 7%">Details of the credit line</h3>

        <div class="row panel panel-default" style="margin-bottom:50px">
            <div class="panel-heading">
                <h4>Information about accounting entry</h4>
            </div>
            <table class="table">
                <tr>
                    <td>
                        <br/><b>Account number:</b> ${account.numberAccount}
                        <br/><b>Type account:</b> ${account.typeAccount}
                        <br/><b>Credit:</b> ${account.credit}
                        <br/><b>Deposit:</b> ${account.deposit}
                    </td>
                </tr>
            </table>
        </div>

        <!---block with information about agreement--->
        <div class="row panel panel-default">
            <div class="panel-heading">
                <h4>Information about agreement #${account.cred.numberAgreement}</h4>
            </div>
            <table class="table table-stripped">
                <tr>
                    <td><b>Type of deposit</b></td>
                    <td>${account.cred.type}</td>
                    <td><b>First name</b></td>
                    <td>${account.client.firstname}</td>
                </tr>
                <tr>
                    <td><b>Name of deposit</b></td>
                    <td>${account.cred.name}</td>
                    <td><b>Middle name</b></td>
                    <td>${account.client.middlename}</td>
                </tr>
                <tr>
                    <td><b>Amount of deposit</b></td>
                    <td>${account.cred.sum}</td>
                    <td><b>Last name</b></td>
                    <td>${account.client.lastname}</td>
                </tr>
                <tr>
                    <td><b>Currency</b></td>
                    <td>${account.cred.currency.name}</td>
                    <td><b>Identification number</b></td>
                    <td>${account.client.identityNumber}</td>
                </tr>
                <tr>
                    <td><b>Percent of deposit</b></td>
                    <td>${account.cred.percent}</td>
                    <td><b>Home phone</b></td>
                    <td>${account.client.phoneNumberHome}</td>
                </tr>
                <tr>
                    <td><b>Duration (days)</b></td>
                    <td>${account.cred.duration}</td>
                    <td><b>Mobile phone</b></td>
                    <td>${account.client.phoneNumberMobile}</td>
                </tr>
                <tr>
                    <td><b>Start date</b></td>
                    <td>${account.cred.start}</td>
                    <td><b>E-mail</b></td>
                    <td>${account.client.email}</td>
                </tr>
                <tr>
                    <td><b>Expiration date</b></td>
                    <td>${account.cred.extend}</td>
                    <td><b>City of residence</b></td>
                    <td>${account.client.residenceCity}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>
</html>