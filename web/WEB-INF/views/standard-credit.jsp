<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Banking online</title>
    <link href="../../resources/css/bootstrap.css" rel="stylesheet" >
    <link href="../../resources/css/style.css" rel="stylesheet">
    <script src="../../resources/js/jquery-3.1.1.min.js"></script>
    <script src="../../resources/js/bootstrap.js"></script>
    <script src="../../resources/js/jquery.mask.js"></script>
</head>

<body>
<%@include file='menu.jsp'%>

<div class="row">
    <form:form method="post" action="standard-credit-new" modelAttribute="credit">
        <div class="container">
            <div class="starter-template">
                <div class="row">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-1">
                            <div class="form-group">
                                <label for="numberAgreement">Agreement #:</label>
                                <form:input pattern="^[A-Za-z]{1,100}$" id="numberAgreement" type="number" required="required" class="form-control" path="numberAgreement"/>
                            </div>
                        </div>

                        <div class="col-md-4 col-md-offset-1">
                            <label for="currency">Currency:</label>
                            <form:select id="currency" required="required" class="form-control" path="currency.id">
                                <c:forEach items="${listCurrencies}" var="currency">
                                    <form:option value="${currency.id}">
                                        ${currency.name}
                                    </form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-1">
                            <div class="form-group">
                                <label for="typeOfCredit">Type of credit:</label>
                                <form:input pattern="^[A-Za-z]{1,100}$" id="typeOfCredit" type="text" required="required" class="form-control" path="type" value="standard" readonly="true"/>
                            </div>
                        </div>

                        <div class="col-md-4 col-md-offset-1">
                            <div class="form-group">
                                <label for="startDate">Start date:</label>
                                <form:input pattern="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" id="startDate" type="text" required="required" class="form-control datemask" onkeyup="setExtendDate()" path="start"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-1">
                            <div class="form-group">
                                <label for="nameOfCredit">Name of credit:</label>
                                <form:input pattern="^[A-Za-z]{1,100}$" id="nameOfCredit" type="text" required="required" class="form-control" path="name" value="orange dream standard" readonly="true"/>
                            </div>
                        </div>

                        <div class="col-md-4 col-md-offset-1">
                            <div class="form-group">
                                <label for="extendDate">Expiration date:</label>
                                <form:input pattern="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" id="extendDate" type="text" required="required" class="form-control datemask" path="extend" readonly="true"/>
                                <script type="text/javascript">
                                    function setExtendDate () {
                                        var splits = $('#startDate').val().split('/');
                                        if (splits[2])
                                            $('#extendDate').val(splits[0] + '/' + splits[1] + '/' + (+splits[2] + 1));
                                    }
                                </script>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-1">
                            <div class="form-group">
                                <label for="sumOfCredit" >Amount of credit (500-30000):</label>
                                <form:input pattern="^[A-Za-z]{1,100}$" id="sumOfCredit" min="500" max="30000" type="number" required="required" class="form-control" path="sum"/>
                            </div>
                        </div>

                        <div class="col-md-4 col-md-offset-1">
                            <div class="form-group">
                                <label for="percent">Percent of credit:</label>
                                <form:input pattern="^[A-Za-z]{1,100}$" id="percent" type="text" required="required" class="form-control" path="percent" value="23" readonly="true"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-1">
                            <div class="form-group">
                                <label for="plan">Plans of accounts:</label>
                                <form:select id="plan" required="required" class="form-control" path="accountPlanID">
                                    <c:forEach items="${listAccountPlans}" var="accountPlan">
                                        <form:option value="${accountPlan.id}">
                                            ${accountPlan.codePlan}
                                        </form:option>
                                    </c:forEach>
                                </form:select>
                            </div>
                        </div>

                        <div class="col-md-4 col-md-offset-1">
                            <div class="form-group">
                                <label for="termOfCredit">Duration (month):</label>
                                <form:input pattern="^[A-Za-z]{1,100}$" id="termOfCredit" type="number" required="required" class="form-control" path="duration" value="12" readonly="true"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-md-offset-1">
                            <div class="form-group">
                                <label for="client">Client:</label>
                                <form:select id="client" required="required" class="form-control" path="client.id">
                                    <c:forEach items="${listClients}" var="client">
                                        <form:option value="${client.id}">
                                            ${client.getFullname()}
                                            ${client.identityNumber}
                                        </form:option>
                                    </c:forEach>
                                </form:select>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-4 col-md-offset-6" style="margin-top: 2%">
                <form:button type="submit" class="btn btn-primary add-btn">Save changes</form:button>
            </div>
        </div>

    </form:form>

</div>
<br/>

</body>
</html>