<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Banking online</title>
    <link href="../../resources/css/bootstrap.css" rel="stylesheet" >
    <link href="../../resources/css/style.css" rel="stylesheet">
    <script src="../../resources/js/jquery-3.1.1.min.js"></script>
    <script src="../../resources/js/bootstrap.js"></script>
    <script src="../../resources/js/jquery.mask.js"></script>
</head>

<body>
<%@include file='menu.jsp'%>

<div class="row">
    <form:form method="POST" action="demand-deposit-new" modelAttribute="deposit">
    <div class="container">
        <div class="starter-template">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="form-group">
                            <label for="numberAgreement">Agreement #:</label>
                            <form:input pattern="^[A-Za-z]{1,100}$" id="numberAgreement" type="number" required="required" class="form-control" path="numberAgreement"/>
                        </div>
                    </div>
                    <div class="row">
                        <label for="typeOfDeposit">Type of deposit:</label>
                        <form:input pattern="^[A-Za-z]{1,100}$" id="typeOfDeposit" type="text" required="required" class="form-control" path="type" value="demand" readonly="true"/>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="nameOfDeposit">Name of deposit:</label>
                            <form:input pattern="^[A-Za-z]{1,100}$" id="nameOfDeposit" type="text" required="required" class="form-control" path="name" value="demand" readonly="true"/>
                            <%--<select class="form-control" id="nameOfDeposit" name="addNameDeposit" required>--%>
                                <%--<option value="optimal1">Optimal-1</option>--%>
                                <%--<option value="exclusive">Exclusive</option>--%>
                            <%--</select>--%>
                            <%--<script type="text/javascript">--%>
                                <%--$(document).ready(function() {--%>
                                    <%--$('#nameOfDeposit').on('change', function() {--%>
                                        <%--$('div.for').hide();--%>
                                        <%--$('div.for-' + $(this).val()).show();--%>
                                    <%--});--%>
                                <%--});--%>
                            <%--</script>--%>
                        </div>
                    </div>
                    <div class="row">
                        <label for="sumOfDeposit" >Amount of deposit:</label>
                        <form:input pattern="^[A-Za-z]{1,100}$" min="100" id="sumOfDeposit" type="number" required="required" class="form-control" path="sum"/>
                    </div>
                    <div class="row">
                        <label for="plan">Plans of accounts:</label>
                        <form:select id="plan" required="required" class="form-control" path="accountPlanID">
                            <c:forEach items="${listAccountPlans}" var="accountPlan">
                                <form:option value="${accountPlan.id}">
                                    ${accountPlan.codePlan}
                                </form:option>
                            </c:forEach>
                        </form:select>
                    </div>

                    <div class="row">
                        <label for="client">Client:</label>
                        <form:select id="client" required="required" class="form-control" path="client.id">
                            <c:forEach items="${listClients}" var="client">
                                <form:option value="${client.id}">
                                    ${client.getFullname()}
                                    ${client.identityNumber}
                                </form:option>
                            </c:forEach>
                        </form:select>
                    </div>

                </div><!--end general information-->

                <div class="col-md-5 col-md-offset-1">

                    <div class="row">
                        <label for="currency">Currency:</label>
                        <form:select id="currency" required="required" class="form-control" path="currency.id">
                            <c:forEach items="${listCurrencies}" var="currency">
                                <form:option value="${currency.id}">
                                    ${currency.name}
                                </form:option>
                            </c:forEach>
                        </form:select>
                    </div>

                    <div class="row">
                        <label for="startDate">Start date:</label>
                        <form:input pattern="^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$" id="startDate" type="text" required="required" class="form-control datemask" path="start"/>
                    </div>

                    <div class="for-optimal1 for">
                        <div class="row">
                            <label for="percent">Percent of deposit:</label>
                            <form:input pattern="^[A-Za-z]{1,100}$" id="percent" type="text" required="required" class="form-control" path="percent" value="2.6" readonly="true"/>
                        </div>
                    </div><!--end block for deposit optimal1-->

                </div><!---end third column---->

                <div class="row">
                    <div class="col-md-4 col-md-offset-6">
                        <form:button type="submit" class="btn btn-primary add-btn">Save changes</form:button>
                    </div>
                </div>
            </div>

            </form:form>
        </div>
        <br/>

</body>
</html>

