<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Banking online</title>
    <link href="../../resources/css/bootstrap.css" rel="stylesheet" >
    <link href="../../resources/css/style.css" rel="stylesheet">
    <script src="../../resources/js/jquery-3.1.1.min.js"></script>
    <script src="../../resources/js/bootstrap.js"></script>
    <script src="../../resources/js/jquery.mask.js"></script>
</head>
<body>
<div class="container">
    <div class="starter-template">
        <h3 class="text-center" style="margin-bottom: 7%">Details of the credit account</h3>

        <div class="row panel panel-default" style="margin-bottom:20px">
            <div class="panel-heading">
                <h4>Information about accounting entry</h4>
            </div>
            <table class="table">
                <tr>
                    <td>
                        <br/><b>Account number:</b> ${account.numberAccount}
                        <br/><b>Type account:</b> ${account.typeAccount}
                        <br/><b>Balance:</b> ${account.balance}
                        <br/><b>Currency:</b> ${account.cred.currency.name}
                    </td>
                </tr>
            </table>
            <a href="print-account?id=${account.id}" role="button" class="btn btn-primary col-md-1">Print</a>
            <a href="personal-account" role="button" class="btn btn-danger col-md-1">Go Back</a>
        </div>
    </div>
</div>
</body>
</html>