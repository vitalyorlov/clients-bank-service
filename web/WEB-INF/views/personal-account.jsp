<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Banking online</title>
    <link href="../../resources/css/bootstrap.css" rel="stylesheet" >
    <link href="../../resources/css/style.css" rel="stylesheet">
    <script src="../../resources/js/jquery-3.1.1.min.js"></script>
    <script src="../../resources/js/bootstrap.js"></script>
    <script src="../../resources/js/jquery.mask.js"></script>
</head>

<body>

<div class="row">
    <div class="text-center">
        <div class="deposit-block">
            <c:if test="${not empty successMessage}">
                <br>
                <div class="row">
                    <div class="alert alert-success col-md-offset-1 col-md-10">
                        <strong>Success!</strong> ${successMessage}
                    </div>
                </div>
            </c:if>
            <c:if test="${not empty errorMessage}">
                <br>
                <div class="row">
                    <div class="alert alert-danger col-md-offset-1 col-md-10">
                        <strong>Danger!</strong> ${errorMessage}
                    </div>
                </div>
            </c:if>
            <div class="row">
                <h3 class="text-center">Please, select the operation:</h3>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <a href="get-money" role="button" class="btn btn-block btn-default">Get Money</a>
                </div>
            </div>
            <div class="row">
                <h4 class="text-center">----</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <a href="get-credit-account" role="button" class="btn btn-block btn-default">Show the rest of credit account</a>
                </div>
            </div>
            <div class="row">
                <h4 class="text-center">----</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <a href="get-deposit-accounts" role="button" class="btn btn-block btn-default">Show the deposits</a>
                </div>
            </div>
            <div class="row">
                <h4 class="text-center">----</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <a href="logout" role="button" class="btn btn-block btn-default">Logout</a>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>

</body>
</html>
