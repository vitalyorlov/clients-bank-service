<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Banking online</title>
    <link href="../../resources/css/bootstrap.css" rel="stylesheet" >
    <link href="../../resources/css/style.css" rel="stylesheet">
    <script src="../../resources/js/jquery-3.1.1.min.js"></script>
    <script src="../../resources/js/bootstrap.js"></script>
    <script src="../../resources/js/jquery.mask.js"></script>
</head>

<body>
  <%@include file='menu.jsp'%>
  <%@include file='clients-add-modal.jsp'%>
  <%@include file='clients-edit-modal.jsp'%>
  <%@include file='clients-delete-modal.jsp'%>

  <c:if test="${not empty errorMessage}">
    <br>
    <div class="row">
      <div class="alert alert-danger col-md-offset-1 col-md-10">
        <strong>Danger!</strong> ${errorMessage}
      </div>
    </div>
  </c:if>

  <h2 class="col-md-offset-1">List of Clients</h2>
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="table-responsive">
          <table class="table table-striped table-bordered">
          <thead class="thead-inverse">
            <tr>
              <th>first name</th>
              <th>last name</th>
              <th>middle name</th>
              <th>birth date</th>
              <th>passport series</th>
              <th>passport number</th>
              <th>issued by</th>
              <th>issued date</th>
              <th>identity number</th>
              <th>birth place</th>
              <th>living city</th>
              <th>living address</th>
              <th>home phone number</th>
              <th>mobile phone number</th>
              <th>email</th>
              <th>the post</th>
              <th>retired</th>
              <th>workplace</th>
              <th>income</th>
              <th>citizenship</th>
              <th>residence city</th>
              <th>disability</th>
              <th>marital status</th>
              <th>actions</th>
            </tr>
          </thead>
          <tbody>
            <c:forEach items="${listClients}" var="client">
              <tr>
                <td>${client.firstname}</td>
                <td>${client.lastname}</td>
                <td>${client.middlename}</td>
                <td>
                  <fmt:formatDate pattern="dd/MM/yyyy"
                                  value="${client.birthday}" />
                </td>
                <td>${client.passportSeries}</td>
                <td>${client.passportNumber}</td>
                <td>${client.issuedBy}</td>
                <td>
                  <fmt:formatDate pattern="dd/MM/yyyy"
                                  value="${client.issuedDate}" />
                </td>
                <td>${client.identityNumber}</td>
                <td>${client.birthplace}</td>
                <td>${client.livingCity.name}</td>
                <td>${client.livingAddress}</td>
                <td>${client.phoneNumberHome}</td>
                <td>${client.phoneNumberMobile}</td>
                <td>${client.email}</td>
                <td>${client.thePost}</td>
                <td>${client.retired ? "Yes" : "No"}</td>
                <td>${client.workplace}</td>
                <td>${client.income}</td>
                <td>${client.citizenship.name}</td>
                <td>${client.residenceCity.name}</td>
                <td>${client.disability.name}</td>
                <td>${client.maritalStatus.name}</td>
                <td>

                  <button class="btn btn-link btn-sm"
                          id="${client.id}"
                          onclick="showEditModal(this)">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    update
                  </button>
                  &middot;
                  <button class="btn btn-link btn-sm" id_instance="${client.id}"
                          onclick="showDeleteModal(this)">
                    <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                    delete
                  </button>

                </td>
              </tr>
            </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <br/>

  <button class="btn btn-default btn-sm col-md-offset-1" onclick="showAddModal()">
    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
    Add new client
  </button>

  <script>
    $(document).ready(function () {
      $('.datemask').mask("00/00/0000", {placeholder: "__/__/____"});
      $('.identity').mask("AAAAAAAAAAAAAA", {placeholder: "______________"});
      $('.passnumber').mask("0000000", {placeholder: "_______"});
      $('.homephone').mask("00-00-00", {placeholder: "__-__-__"});
      $('.mobilephone').mask("+375000000000", {placeholder: "+375_________"});
    });

    function showAddModal()
    {
      $('.clients_add_modal').modal();
    }

    function showEditModal(instance)
    {
      var id = $(instance).attr('id');
      $('#edit_form').attr('action', 'client/edit/' + id);
      $('#clients_edit_id').val(id);
      getValues(id);
      $('.clients_edit_modal').modal();
    }

    function showDeleteModal(instance)
    {
      var id = $(instance).attr('id_instance');
      $('#delete_form').attr('action', 'client/remove/' + id);
      $('.clients_delete_modal').modal();
    }

    function getValues(id) {
      $.ajax({
        url: 'get-client-info',
        type: "get",
        contentType: "application/json",
        data: {"id" : id},
        dataType: 'json',
        success: function(data){
          var client = data;

          $('#clients_edit_firstname').val(client.firstname);
          $('#clients_edit_lastname').val(client.lastname);
          $('#clients_edit_middlename').val(client.middlename);
          $('#clients_edit_birthdate').val(client.birthday);
          $('#clients_edit_passport_series').val(client.passportSeries);
          $('#clients_edit_passport_number').val(client.passportNumber);
          $('#clients_edit_issued_by').val(client.issuedBy);
          $('#clients_edit_issued_date').val(client.issuedDate);
          $('#clients_edit_identity_number').val(client.identityNumber);
          $('#clients_edit_birth_place').val(client.birthplace);
          $('#clients_edit_living_address').val(client.livingAddress);
          $('#clients_edit_home_phone_number').val(client.phoneNumberHome);
          $('#clients_edit_mobile_phone_number').val(client.phoneNumberMobile);
          $('#clients_edit_email').val(client.email);
          $('#clients_edit_the_post').val(client.thePost);
          $('#clients_edit_work_place').val(client.workplace);
          $('#clients_edit_income').val(client.income);
          $('#clients_edit_living_city_id option').removeAttr('selected');
          $('#clients_edit_living_city_id option[value=' + client.livingCityId + ']').attr('selected', 'selected');
          $('#clients_edit_residence_city_id option').removeAttr('selected');
          $('#clients_edit_residence_city_id option[value=' + client.residenceCityId + ']').attr('selected', 'selected');
          $('#clients_edit_marital_status_id option').removeAttr('selected');
          $('#clients_edit_marital_status_id option[value=' + client.maritalStatusId + ']').attr('selected', 'selected');
          $('#clients_edit_disability_id option').removeAttr('selected');
          $('#clients_edit_disability_id option[value=' + client.disabilityId + ']').attr('selected', 'selected');
          $('#clients_edit_citizenship_id option').removeAttr('selected');
          $('#clients_edit_citizenship_id option[value=' + client.citizenshipId + ']').attr('selected', 'selected');
          client.retired ? $('#clients_edit_retired_true').attr('checked', 'checked')
                  : $('#clients_edit_retired_false').attr('checked', 'checked');
        }
      });

    }
  </script>

</body>
</html>
