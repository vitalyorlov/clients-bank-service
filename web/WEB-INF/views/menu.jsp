<text class="menu">
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color: #2c3e50;">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/"><span class="glyphicon glyphicon-th"></span></a>
            </div>
            <div class="collapse navbar-collapse ">
                <ul class="nav navbar-nav">
                    <li><a href="/">Client base</a></li>
                    <li><a href="deposits">Deposits</a></li>
                    <li><a href="create-deposit">New deposit</a></li>
                    <li><a href="credits">Credit base</a></li>
                    <li><a href="create-credit">New credit</a></li>
                    <li><a href="close-day-for-deposits">Close day for deposits</a></li>
                    <li><a href="close-day-for-credits">Close day for credits</a></li>
                    <li><a href="login">Login</a></li>
                </ul>
                <%--<ul class=" nav navbar-nav navbar-right">--%>
                    <%--<li><a href="autorization.php">Login</a></li>--%>
                <%--</ul>--%>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</text>
