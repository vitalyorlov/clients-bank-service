<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Banking online</title>
    <link href="../../resources/css/bootstrap.css" rel="stylesheet" >
    <link href="../../resources/css/style.css" rel="stylesheet">
    <script src="../../resources/js/jquery-3.1.1.min.js"></script>
    <script src="../../resources/js/bootstrap.js"></script>
    <script src="../../resources/js/jquery.mask.js"></script>
</head>

<body>
<%@include file='menu.jsp'%>

<div class="row">
    <div class="text-center">
        <div class="deposit-block">
            <div class="row">
                <h4 class="text-center">Please, select the credit line:</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <a href="annuity-credit" role="button" class="btn btn-block btn-default">Credit with a monthly repayment of debt annuity payments</a>
                </div>
            </div>
            <div class="row">
                <h4 class="text-center">OR</h4>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <a href="standard-credit" role="button" class="btn btn-block btn-default">Credit with a monthly repayment of interest on the loan</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<br/>

</body>
</html>
