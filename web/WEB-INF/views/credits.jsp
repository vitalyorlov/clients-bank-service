<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Banking online</title>
    <link href="../../resources/css/bootstrap.css" rel="stylesheet" >
    <link href="../../resources/css/style.css" rel="stylesheet">
    <script src="../../resources/js/jquery-3.1.1.min.js"></script>
    <script src="../../resources/js/bootstrap.js"></script>
    <script src="../../resources/js/jquery.mask.js"></script>
</head>

<body>
<%@include file='menu.jsp'%>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <h4 class="text-left" style="margin-bottom: 2%">Database with credits lines</h4>
        <div class="table-responsive">
            <table class="table table-striped">
                <tr>
                    <th>Number account</th>
                    <th>Code plan</th>
                    <th>Type account</th>
                    <th>Currency</th>
                    <th>Balance</th>
                    <th>Client</th>
                    <th>View</th>
                </tr>
                <c:forEach items="${listAccounts}" var="account">
                <tr>
                    <td> ${account.cred.numberAgreement} </td>
                    <td> ${account.numberAccount}</td>
                    <td> ${account.cred.type} </td>
                    <td> ${account.cred.currency.name} </td>
                    <td> ${account.balance} </td>
                    <td> ${account.cred.client.getFullname()} </td>
                    <td><a href="info-credit?info=${account.id}"><span class="glyphicon glyphicon-edit"></span></a></td>
                </tr>
                </c:forEach>
            </table>
        </div>
    </div>
</div>
<br/>

</body>
</html>

