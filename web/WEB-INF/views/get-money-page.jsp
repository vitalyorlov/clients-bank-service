<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Banking online</title>
    <link href="../../resources/css/bootstrap.css" rel="stylesheet" >
    <link href="../../resources/css/style.css" rel="stylesheet">
    <script src="../../resources/js/jquery-3.1.1.min.js"></script>
    <script src="../../resources/js/bootstrap.js"></script>
    <script src="../../resources/js/jquery.mask.js"></script>
</head>

<body>

<div class="row">
    <div class="text-center">
        <div class="deposit-block">
            <form method="post" action="give-me-money">
                <div class = "form-group col-md-offset-4 col-md-4">
                    <label for="card">Enter the sum:</label>
                    <input id="card" type="number" required="required" class="form-control" name="sum"/>
                </div>
                <button type="submit" class="btn btn-primary col-md-offset-4 col-md-2">Get Money</button>
                <a href="personal-account" role="button" class="btn btn-danger col-md-2">Cancel</a>
            </form>
        </div>
    </div>
</div>
<br/>

</body>
</html>


