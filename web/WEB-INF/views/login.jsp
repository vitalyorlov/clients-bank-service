<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Banking online</title>
    <link href="../../resources/css/bootstrap.css" rel="stylesheet" >
    <link href="../../resources/css/style.css" rel="stylesheet">
    <script src="../../resources/js/jquery-3.1.1.min.js"></script>
    <script src="../../resources/js/bootstrap.js"></script>
    <script src="../../resources/js/jquery.mask.js"></script>
</head>

<body>

<div class="row">
    <div class="text-center">
        <c:if test="${not empty errorMessage}">
            <br>
            <div class="row">
                <div class="alert alert-danger col-md-offset-1 col-md-10">
                    <strong>Danger!</strong> ${errorMessage}
                </div>
            </div>
        </c:if>
        <div class="deposit-block">
            <form method="post" action="authorizate">
                <div class="row">
                    <h4 class="text-center">Please, enter credentials:</h4>
                </div>
                <div class = "form-group col-md-offset-4 col-md-4">
                    <label for="card">Number a  credit card:</label>
                    <input pattern="^[0-9]{16}$" id="card" type="text" required="required" class="form-control" name="numberCard"/>
                </div>
                <div class = "form-group col-md-offset-4 col-md-4">
                    <label for="pin">Pin-code:</label>
                    <input pattern="^[0-9]{4}$" id="pin" type="password" required="required" class="form-control" name="pinCode"/>
                </div>
                <button type="submit" class="btn btn-primary col-md-offset-4 col-md-2">Log in</button>&nbsp;
                <a href="/" role="button" class="btn btn-danger col-md-2">Go back</a>
            </form>
        </div>
    </div>
</div>
<br/>

</body>
</html>

