/**
 * Created by Violka on 17.09.2015.
 */

jQuery(document).ready(function() {
        //for addUser.html
        jQuery("#addNumberPassports").mask("999999999");
        jQuery("#addSeriesPassports").mask("aa");
        jQuery("#addIdentificationNumber").mask("9999999-a-999-aa-9");
        jQuery("#addHomePhone").mask("9(999)99-99-999");
        jQuery("#addMobilePhone").mask("9(999)99-99-999");

        //for editUser.html
        jQuery("#editNumberPassports").mask("999999999");
        jQuery("#editSeriesPassports").mask("aa");
        jQuery("#editIdentificationNumber").mask("9999999-a-999-aa-9");
        jQuery("#editHomePhone").mask("9(999)99-99-999");
        jQuery("#editMobilePhone").mask("9(999)99-99-999");

        //for onlineBanking.html
        jQuery("#phone").mask("9(999)99-99-999");

});

